{-# OPTIONS --cubical --allow-unsolved-metas --termination-depth=10 #-}

-- NOTE: This file is work in progress and NOT part of the main
-- development.


module Sierpinski where

open import Cubical.Foundations.Prelude hiding (_∨_; _∧_)
open import Cubical.Foundations.Path
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Univalence
open import Cubical.Foundations.Structure

open import Cubical.Data.Nat
open import Cubical.Data.Sum
open import Cubical.Data.Sigma hiding (_∨_; _∧_)
open import Cubical.Data.Empty as Empty renaming (⊥ to Empty)
open import Cubical.Data.Unit
open import Cubical.Relation.Nullary


open import Cubical.HITs.PropositionalTruncation
open import Cubical.HITs.PropositionalTruncation.Properties
  renaming (rec to ∥-∥rec)

open import Iff

mutual

  data Sierpinski : Type where
    ⊤ : Sierpinski
    ⊥ : Sierpinski
    ⋁ : (ℕ → Sierpinski) -> Sierpinski
    ∨-comm : ∀ {x y} → x ∨ y ≡ y ∨ x
    ∨-assoc : ∀ {x y z} → x ∨ (y ∨ z) ≡ (x ∨ y) ∨ z
    ∨-idem : ∀ {x} → x ∨ x ≡ x
    ∨-bot : ∀ {x} → x ∨ ⊥ ≡ x
    ∨-absorb : ∀ {s : ℕ → Sierpinski} n → s n ∨ (⋁ s) ≡ ⋁ s
    ∨-distr : ∀ {s : ℕ → Sierpinski} {x} → x ∨ (⋁ s) ≡ ⋁ (λ n → x ∨ s n)
    isSetSierpinski : isSet Sierpinski

  _∨_ : Sierpinski → Sierpinski → Sierpinski
  x ∨ y = ⋁ (caseNat x y)


module _ (P : Sierpinski → Type)(isProp⟨P⟩ : (x : Sierpinski) → isProp (P x))
         (p⊤ : P ⊤)(p⊥ : P ⊥)(p⋁ : ∀ {s} → P (s 0) → ((n : ℕ) -> P (s (suc n))) → P (⋁ s))
         -- we split up the p⋁ case into head and tail to please the termination checker, due to the `caseNat`s involved
       where

  Sierpinski-ind' : (x : Sierpinski) -> P x
  Sierpinski-ind' ⊤ = p⊤
  Sierpinski-ind' ⊥ = p⊥
  Sierpinski-ind' (⋁ s) = p⋁ (Sierpinski-ind' (s 0)) (λ n → Sierpinski-ind' (s (suc n)))
  Sierpinski-ind' (∨-comm {x} {y} i) = isProp→PathP (λ i → isProp⟨P⟩ (∨-comm i)) (p⋁ (Sierpinski-ind' x) (λ n → Sierpinski-ind' y)) (p⋁ (Sierpinski-ind' y) (λ n → Sierpinski-ind' x)) i
  Sierpinski-ind' (∨-assoc {x} {y} {z} i) = isProp→PathP (λ i → isProp⟨P⟩ (∨-assoc i)) (p⋁ (Sierpinski-ind' x) (λ _ → p⋁ (Sierpinski-ind' y) (λ _ → Sierpinski-ind' z))) (p⋁ (p⋁ (Sierpinski-ind' x) (λ n → Sierpinski-ind' y)) (λ n → Sierpinski-ind' z)) i
  Sierpinski-ind' (∨-idem {x} i) = isProp→PathP (λ i → isProp⟨P⟩ (∨-idem i)) (p⋁ (Sierpinski-ind' x) (λ n → Sierpinski-ind' x)) (Sierpinski-ind' x) i
  Sierpinski-ind' (∨-bot {x} i) = isProp→PathP (λ i → isProp⟨P⟩ (∨-bot i)) (p⋁ (Sierpinski-ind' x) (λ n → p⊥)) (Sierpinski-ind' x) i
  Sierpinski-ind' (∨-absorb {s} n i) = isProp→PathP (λ i → isProp⟨P⟩ (∨-absorb n i)) (p⋁ (Sierpinski-ind' (s n)) (λ _ → p⋁ (Sierpinski-ind' (s 0)) (λ k → Sierpinski-ind' (s (suc k))))) (p⋁ (Sierpinski-ind' (s 0)) (λ n → Sierpinski-ind' (s (suc n)))) i
  Sierpinski-ind' (∨-distr {s} {x} i) = isProp→PathP (λ i → isProp⟨P⟩ (∨-distr i)) (p⋁ (Sierpinski-ind' x)(λ _ → p⋁ (Sierpinski-ind' (s 0)) (λ n → Sierpinski-ind' (s (suc n))))) (p⋁ (p⋁ (Sierpinski-ind' x) (λ n → Sierpinski-ind' (s 0))) (λ n → p⋁ (Sierpinski-ind' x) (λ n₁ → Sierpinski-ind' (s (suc n))))) i
  Sierpinski-ind' (isSetSierpinski x y p q i j) = isProp→SquareP (λ i j → isProp⟨P⟩ (isSetSierpinski x y p q i j)) (λ j → Sierpinski-ind' x) (λ j → Sierpinski-ind' y) (λ j → Sierpinski-ind' (p j)) (λ j → Sierpinski-ind' (q j)) i j

-- Of course now we can give the "proper" type for `p⋁`, post facto

Sierpinski-ind : (P : Sierpinski → Type)(isProp⟨P⟩ : (x : Sierpinski) → isProp (P x))
                 (p⊤ : P ⊤)
                 (p⊥ : P ⊥)
                 (p⋁ : ∀ {s} → ((n : ℕ) -> P (s n)) → P (⋁ s))
                 (x : Sierpinski) -> P x
Sierpinski-ind P isProp⟨P⟩ p⊤ p⊥ p⋁ x = Sierpinski-ind' P isProp⟨P⟩ p⊤ p⊥ (λ {s} p0 psuc → p⋁ λ { zero → p0 ; (suc n) → psuc n }) x

⊤-is-top : (x : Sierpinski) → x ∨ ⊤ ≡ ⊤
⊤-is-top = Sierpinski-ind (λ x → x ∨ ⊤ ≡ ⊤) (λ x → isSetSierpinski (x ∨ ⊤) ⊤)
                          ∨-idem
                          (∨-comm ∙ ∨-bot)
                          λ {s} ih → ∨-comm ∙ ∨-distr ∙ cong ⋁ ((funExt (λ n → ∨-comm ∙ ih n)) ∙ funExt (λ { zero → refl ; (suc n) → refl })) ∙ ∨-idem

contains-⊤-implies-is-⊤ : (s : ℕ → Sierpinski) -> ∃[ n ∈ ℕ ] (s n ≡ ⊤) → ⋁ s ≡ ⊤
contains-⊤-implies-is-⊤ s = ∥-∥rec {A = Σ ℕ (λ n → s n ≡ ⊤)} {P = ⋁ s ≡ ⊤} (isSetSierpinski (⋁ s) ⊤)
                                   λ { (n , sn=⊤) → sym (∨-absorb n) ∙ ∨-comm ∙ cong (λ z → ⋁ s ∨ z) sn=⊤ ∙ ⊤-is-top (⋁ s) }

⋁-idem : ∀ {x} → ⋁ (λ _ → x) ≡ x
⋁-idem {x} = ⋁x≡x∨x ∙ ∨-idem where
  ⋁x≡x∨x : ⋁ (λ _ → x) ≡ x ∨ x
  ⋁x≡x∨x = cong ⋁ (funExt λ { zero → refl ; (suc n) → refl })

{-
-- Same termination checker trick for `is⊥` -- not very interesting
is⊥' : Sierpinski -> hProp ℓ-zero
is⊥' ⊤ = Empty , isProp⊥
is⊥' ⊥ = Unit , isPropUnit
is⊥' (⋁ s) = ∥ typ (is⊥' (s 0)) ∥ × (∀ n → ∥ typ (is⊥' (s (suc n))) ∥) , isProp× isPropPropTrunc (isPropΠ (λ n → isPropPropTrunc))
is⊥' (∨-comm {x} {y} i) = Σ≡Prop (λ A → isPropIsProp) {Σ ∥ fst (is⊥' x) ∥ (λ _ → (n : ℕ) → ∥ fst (is⊥' y) ∥) , isProp× isPropPropTrunc (isPropΠ (λ n → isPropPropTrunc))} {Σ ∥ fst (is⊥' y) ∥ (λ _ → (n : ℕ) → ∥ fst (is⊥' x) ∥) , isProp× isPropPropTrunc (isPropΠ (λ n → isPropPropTrunc))} (hPropExt (isProp× isPropPropTrunc (isPropΠ (λ n → isPropPropTrunc))) (isProp× isPropPropTrunc (isPropΠ (λ n → isPropPropTrunc)))
                                  (λ { (ix , f) → f 1 , λ _ → ix })
                                  (λ { (ix , f) → f 1 , λ _ → ix })) i
is⊥' (∨-assoc i) = {!!}
is⊥' (∨-idem i) = {!!}
is⊥' (∨-bot i) = {!!}
is⊥' (∨-absorb n i) = {!!}
is⊥' (∨-distr i) = {!!}
is⊥' (isSetSierpinski x x₁ x₂ y i i₁) = {!!}
-}


{-# TERMINATING #-}
is⊥ : Sierpinski -> hProp ℓ-zero
is⊥ ⊤ = Empty , isProp⊥
is⊥ ⊥ = Unit , isPropUnit
is⊥ (⋁ s) = (∀ n → ∥ typ (is⊥ (s n)) ∥) , isPropΠ (λ n → isPropPropTrunc)
is⊥ (∨-comm {x} {y} i) = Σ≡Prop (λ A → isPropIsProp) {is⊥ (x ∨ y)} {is⊥ (y ∨ x)} (hPropExt (isPropΠ (λ n → isPropPropTrunc)) (isPropΠ (λ n → isPropPropTrunc))
                                  (λ { f zero → f 1 ; f (suc n) → f 0 })
                                  (λ { f zero → f 1 ; f (suc n) → f 0 })) i
is⊥ (∨-assoc {x} {y} {z} i) = Σ≡Prop (λ A → isPropIsProp) {is⊥ (x ∨ (y ∨ z))} {is⊥ ((x ∨ y) ∨ z)}
                                     (hPropExt (isPropΠ (λ n → isPropPropTrunc)) (isPropΠ (λ n → isPropPropTrunc))
                                       (λ f → λ { zero → ∥-∥rec isPropPropTrunc (λ g → ∣ (λ { zero → f 0 ; (suc m) → g 0 }) ∣) (f 1) ; (suc n) → ∥-∥rec isPropPropTrunc (λ g → g 1) (f 1)})
                                       λ f → λ { zero → ∥-∥rec isPropPropTrunc (λ g → g 0) (f 0) ; (suc n) → ∣ (λ { zero → ∥-∥rec isPropPropTrunc (λ g → g 1) (f 0) ; (suc m) → f 1 }) ∣ })
                                     i
is⊥ (∨-idem {x} i) =  Σ≡Prop (λ A → isPropIsProp) {is⊥ (x ∨ x)} {is⊥ x} (hPropExt (isPropΠ (λ n → isPropPropTrunc)) (str (is⊥ x))
                             (λ f → ∥-∥rec (str (is⊥ x)) (λ x → x) (f 0))
                             λ p → λ { zero → ∣ p ∣ ; (suc n) → ∣ p ∣ }) i
is⊥ (∨-bot {x} i) = Σ≡Prop (λ A → isPropIsProp) {is⊥ (x ∨ ⊥)} {is⊥ x} (hPropExt (isPropΠ (λ n → isPropPropTrunc)) (str (is⊥ x))
                           (λ f →  ∥-∥rec (str (is⊥ x)) (λ x → x) (f 0))
                           λ p → λ { zero → ∣ p ∣ ; (suc n) → ∣ tt ∣ }) i
is⊥ (∨-absorb {s} n i) = Σ≡Prop (λ A → isPropIsProp) {is⊥ (s n ∨ ⋁ s)} {is⊥ (⋁ s)} (hPropExt (isPropΠ (λ n → isPropPropTrunc)) (isPropΠ (λ n → isPropPropTrunc))
                                (λ f k → ∥-∥rec isPropPropTrunc (λ g → g k) (f 1))
                                λ g → λ { zero → g n ; (suc k) → ∣ (λ m → g m) ∣ }) i
is⊥ (∨-distr {s} {x} i) = Σ≡Prop (λ A → isPropIsProp) {is⊥ (x ∨ ⋁ s)} {is⊥ (⋁ (λ n → x ∨ s n))} (hPropExt (isPropΠ (λ n → isPropPropTrunc)) (isPropΠ (λ n → isPropPropTrunc))
                                 (λ f n → ∣ (λ { zero → f 0 ; (suc m) → ∥-∥rec isPropPropTrunc (λ g → g n) (f 1) }) ∣ )
                                 λ f → λ { zero → ∥-∥rec isPropPropTrunc (λ g → g 0) (f 0) ; (suc n) → ∣ (λ m → ∥-∥rec isPropPropTrunc (λ g → g 1) (f m)) ∣ }) i
is⊥ (isSetSierpinski x y p q i j) = isSet→SquareP {A = λ _ _ → hProp ℓ-zero} (λ _ _ → isSetHProp) (λ j → is⊥ (p j)) (λ j → is⊥ (q j)) refl refl i j


⊥≠⊤ : ⊥ ≡ ⊤ → Empty
⊥≠⊤ p = subst (λ x → typ (is⊥ x)) p tt

is⊥-correct : ∀ {x} → typ (is⊥ x) → x ≡ ⊥
is⊥-correct {x} = Sierpinski-ind (λ x → typ (is⊥ x) → x ≡ ⊥) (λ x → isProp→ (isSetSierpinski x ⊥))
                Empty.rec
                (λ _ → refl)
                (λ {s} ih f → let s≡⊥ : s ≡ (λ n → ⊥)
                                  s≡⊥ = funExt λ n → ∥-∥rec (isSetSierpinski (s n) ⊥) (ih n) (f n) in subst (λ z → ⋁ z ≡ ⊥) (sym s≡⊥) ⋁-idem)
                x

is⊥-correct⁻¹ : ∀ {x} → x ≡ ⊥ → typ (is⊥ x)
is⊥-correct⁻¹ {x} x≡⊥ = subst (λ z → typ (is⊥ z)) (sym x≡⊥) tt


{-# TERMINATING #-}
is⊤ : Sierpinski -> hProp ℓ-zero
is⊤ ⊤ = Unit , isPropUnit
is⊤ ⊥ = Empty , isProp⊥
is⊤ (⋁ s) = (∃[ n ∈ ℕ ] (typ (is⊤ (s n)))) , isPropPropTrunc
is⊤ (∨-comm {x} {y} i) = Σ≡Prop (λ A → isPropIsProp) {is⊤ (x ∨ y)} {is⊤ (y ∨ x)} (hPropExt isPropPropTrunc isPropPropTrunc
                                (∥-∥rec isPropPropTrunc (λ { (zero , p) → ∣ 1 , p ∣ ; (suc n , p) → ∣ 0 , p ∣ }))
                                (∥-∥rec isPropPropTrunc (λ { (zero , p) → ∣ 1 , p ∣ ; (suc n , p) → ∣ 0 , p ∣ }))) i
is⊤ (∨-assoc {x} {y} {z} i) = Σ≡Prop (λ A → isPropIsProp) {is⊤ (x ∨ (y ∨ z))} {is⊤ ((x ∨ y) ∨ z)} (hPropExt isPropPropTrunc isPropPropTrunc
                                     (∥-∥rec isPropPropTrunc (λ { (zero , p) → ∣ 0 , ∣ 0 , p ∣ ∣ ; (suc n , p) → ∥-∥rec isPropPropTrunc (λ { (zero , q) → ∣ 0 , ∣ 1 , q ∣ ∣ ; (suc m , q) → ∣ 1 , q ∣ }) p }))
                                     (∥-∥rec isPropPropTrunc λ { (zero , p) → ∥-∥rec isPropPropTrunc (λ { (zero , q) → ∣ 0 , q ∣ ; (suc m , q) → ∣ 1 , ∣ 0 , q ∣ ∣ }) p ; (suc n , p) → ∣ 1 , ∣ 1 , p ∣ ∣ })) i
is⊤ (∨-idem {x} i) = Σ≡Prop (λ A → isPropIsProp) {is⊤ (x ∨ x)} {is⊤ x} (hPropExt isPropPropTrunc (str (is⊤ x))
                            (∥-∥rec (str (is⊤ x)) (λ { (zero , p) → p ; (suc n , p) → p }))
                            λ p → ∣ 0 , p ∣) i
is⊤ (∨-bot {x} i) = Σ≡Prop (λ A → isPropIsProp) {is⊤ (x ∨ ⊥)} {is⊤ x} (hPropExt isPropPropTrunc (str (is⊤ x))
                           (∥-∥rec (str (is⊤ x)) (λ { (zero , p) → p }))
                           λ p → ∣ 0 , p ∣) i
is⊤ (∨-absorb {s} n i) = Σ≡Prop (λ A → isPropIsProp) {is⊤ (s n ∨ ⋁ s)} {is⊤ (⋁ s)} (hPropExt isPropPropTrunc isPropPropTrunc
                                (∥-∥rec isPropPropTrunc (λ { (zero , p) → ∣ n , p ∣ ; (suc n , p) → ∥-∥rec isPropPropTrunc (λ { (m , q) → ∣ m , q ∣ }) p }))
                                (∥-∥rec isPropPropTrunc λ { (m , p) → ∣ 1 , ∣ m , p ∣ ∣ })) i
is⊤ (∨-distr {s} {x} i) = Σ≡Prop (λ A → isPropIsProp) {is⊤ (x ∨ ⋁ s)} {is⊤ (⋁ (λ n → x ∨ s n))}  (hPropExt isPropPropTrunc isPropPropTrunc
                                 (∥-∥rec isPropPropTrunc (λ { (zero , p) → ∣ 0 , ∣ 0 , p ∣ ∣ ; (suc n , p) → ∥-∥rec isPropPropTrunc (λ { (m , q) → ∣ m , ∣ 1 , q ∣ ∣ }) p }))
                                 (∥-∥rec isPropPropTrunc λ { (n , p) → ∥-∥rec isPropPropTrunc (λ { (zero , q) → ∣ 0 , q ∣ ; (suc m , q) → ∣ 1 , ∣ n , q ∣ ∣ }) p })) i
is⊤ (isSetSierpinski x y p q i j) = isSet→SquareP {A = λ _ _ → hProp ℓ-zero} (λ _ _ → isSetHProp) (λ j → is⊤ (p j)) (λ j → is⊤ (q j)) refl refl i j


is⊤-correct : ∀ {x} → typ (is⊤ x) → x ≡ ⊤
is⊤-correct {x} = Sierpinski-ind (λ x → typ (is⊤ x) → x ≡ ⊤) (λ x → isProp→ (isSetSierpinski x ⊤))
                                 (λ _ → refl)
                                 (λ ())
                                 (λ {s} ih p → ∥-∥rec (isSetSierpinski (⋁ s) ⊤) (λ { (n , q) → contains-⊤-implies-is-⊤  s ∣ n , ih n q ∣ }) p) x

is⊤-correct⁻¹ : ∀ {x} → x ≡ ⊤ → typ (is⊤ x)
is⊤-correct⁻¹ {x} x≡⊤ = subst (λ z → typ (is⊤ z)) (sym x≡⊤) tt

is-⊤-implies-contains-⊤ : (s : ℕ → Sierpinski) -> ⋁ s ≡ ⊤ → ∃[ n ∈ ℕ ] (s n ≡ ⊤)
is-⊤-implies-contains-⊤ s ⋁s≡⊤ = ∥-∥rec isPropPropTrunc (λ { (n , p) → ∣ n , is⊤-correct p ∣ }) (is⊤-correct⁻¹ ⋁s≡⊤)


-- ## Notation: ≼ for Sierpinski

_≼_ : Sierpinski → Sierpinski → Type
x ≼ y = (x ∨ y) ≡ y

Sierp-antisym : ∀ {x y} → x ≼ y → y ≼ x → x ≡ y
Sierp-antisym {x} {y} x≼y y≼x = x ≡⟨ sym y≼x ⟩ y ∨ x ≡⟨ ∨-comm ⟩ x ∨ y ≡⟨ x≼y ⟩ y ∎

-- duplicate
everything-under-⊤ : ∀ {x} → x ≼ ⊤
everything-under-⊤ = ⊤-is-top _

everything-over-⊥ : ∀ {x} → ⊥ ≼ x
everything-over-⊥ = {!!}

-- duplicate
≼-refl : ∀ {x} → x ≼ x
≼-refl = ∨-idem

≼-trans : ∀ {x y z} → x ≼ y → y ≼ z → x ≼ z
≼-trans {x} {y} {z} x≼y y≼z =
  x ∨ z
    ≡⟨ cong (λ w → x ∨ w) (sym y≼z) ⟩
  (x ∨ (y ∨ z))
    ≡⟨ ∨-assoc ⟩
  ((x ∨ y) ∨ z)
    ≡⟨ cong (λ w → w ∨ z) x≼y ⟩
  (y ∨ z)
    ≡⟨ y≼z ⟩
  z ∎


_≼⟨_⟩_ : ∀ {y z} → (x : Sierpinski) → (x ≼ y) → (y ≼ z) → (x ≼ z)
x ≼⟨ x≼y ⟩ y≼z = ≼-trans x≼y y≼z

_≼∎ : (x : Sierpinski) → x ≼ x
_ ≼∎ = ≼-refl

infixr  0 _≼⟨_⟩_
infix   1 _≼∎

≼-cocone-simple : {s : ℕ → Sierpinski} → ∀ k → s k ≼ ⋁ s
≼-cocone-simple = ∨-absorb

≼-cocone : ∀ {s : ℕ → Sierpinski} {x} k → x ≼ s k → x ≼ ⋁ s
≼-cocone {s} {x} k x≤sk = ≼-trans x≤sk (≼-cocone-simple k)

≼-limiting : ∀ s {x} → ((k : ℕ) → s k ≼ x) → ⋁ s ≼ x
≼-limiting s {x} s≼x = rev-distr ∙ eq-const ∙ ⋁-idem {x}
  where
  rev-distr : ⋁ s ∨ x ≡ ⋁ λ k → s k ∨ x
  rev-distr = (⋁ s ∨ x)
                ≡⟨ ∨-comm ⟩
              (x ∨ ⋁ s)
                ≡⟨ ∨-distr ⟩
              (⋁ λ k → x ∨ s k)
                ≡⟨ cong ⋁ (funExt λ k → ∨-comm) ⟩
              (⋁ λ k → s k ∨ x) ∎
  eq-const : (⋁ λ k → s k ∨ x) ≡ ⋁ λ _ → x
  eq-const = cong ⋁ (funExt λ k → s≼x k)

⋁-on-≼ : {s s' : ℕ → Sierpinski} → (∀ k → s k ≼ s' k) → ⋁ s ≼ ⋁ s'
⋁-on-≼ = {!!}


-- (TODO: many duplicates here. Maybe try to use this notation here to make the below more readable!


-- # Semidecidability

SemiDec : ∀ {ℓ} → Type ℓ → Type ℓ
SemiDec P = Σ[ x ∈ Sierpinski ] (P → x ≡ ⊤) × (x ≡ ⊤ → P)

Dec→SemiDec : ∀ {ℓ}{P : Type ℓ} → Dec P → SemiDec P
Dec→SemiDec (yes p) = ⊤ , (λ _ → refl) , λ _ → p
Dec→SemiDec (no ¬p) = ⊥ , (λ p → Empty.rec (¬p p)) , λ ⊥≡⊤ → Empty.rec (⊥≠⊤ ⊥≡⊤)

eqSemiDec : ∀ {ℓ}{P : Type ℓ} → isProp P → {u v : SemiDec P} -> fst u ≡ fst v → u ≡ v
eqSemiDec {P = P} isProp⟨P⟩ {x , p↔x≡⊤} {y , p↔y≡⊤} x≡y =
  Σ≡Prop {B = λ x → (P → x ≡ ⊤) × (x ≡ ⊤ → P)}
         (λ x → isProp× (isPropΠ λ _ → isSetSierpinski x ⊤) (isPropΠ λ _ → isProp⟨P⟩))
         {u = x , p↔x≡⊤}
         {v = y , p↔y≡⊤}
         x≡y


not⊤-is-⊥ : ∀ y → (y ≡ ⊤ → Empty) → y ≡ ⊥
not⊤-is-⊥ = Sierpinski-ind (λ y → (y ≡ ⊤ → Empty) → y ≡ ⊥) (λ x → isProp→ (isSetSierpinski x ⊥))
             (λ p → Empty.rec (p refl))
             (λ _ → refl)
             λ {s} ih q → is⊥-correct {⋁ s} λ n → ∣ is⊥-correct⁻¹ (ih n (lemma s q n)) ∣
  where
  lemma :  (s : ℕ → Sierpinski) -> (⋁ s ≡ ⊤ → Empty) → (n : ℕ) → s n ≡ ⊤ → Empty
  lemma s p n sn≡⊤ = p (contains-⊤-implies-is-⊤ s ∣ n , sn≡⊤ ∣)

≼-antisym : {x y : Sierpinski} -> (x ≼ y) → (y ≼ x) → x ≡ y
≼-antisym x≼y y≼x = sym y≼x ∙ ∨-comm ∙ x≼y

bisimilar-implies-⋁-equal : {s s' : ℕ → Sierpinski} → ((n : ℕ) → ∃[ m ∈ ℕ ] (s' n ≼ s m)) → ((n : ℕ) → ∃[ m ∈ ℕ ] (s n ≼ s' m)) → ⋁ s ≡ ⋁ s'
bisimilar-implies-⋁-equal {s} {s'} p q = ≼-antisym (≼-limiting s {⋁ s'} (λ k → ∥-∥rec (isSetSierpinski _ _) (λ { (l , sk≼s'l) → ≼-cocone {s'} {s k} l sk≼s'l }) (q k)))
                                                   (≼-limiting s' {⋁ s} (λ k → ∥-∥rec (isSetSierpinski _ _) (λ { (l , s'k≼sl) → ≼-cocone {s} {s' k} l s'k≼sl }) (p k)))

{-
⋁-equal-implies-bisimular : {s s' : ℕ → Sierpinski} → ⋁ s ≡ ⋁ s' → ((n : ℕ) → s n ≡ ⊤ → ∃[ m ∈ ℕ ] (s' m ≡ ⊤)) × ((n : ℕ) → s' n ≡ ⊤ → ∃[ m ∈ ℕ ] (s m ≡ ⊤))
⋁-equal-implies-bisimular {s} {s'} ⋁s≡⋁s' = (⋁-equal-implies-simulation ⋁s≡⋁s' , ⋁-equal-implies-simulation (sym ⋁s≡⋁s'))
  where
    ⋁-equal-implies-simulation : {s s' : ℕ → Sierpinski} → ⋁ s ≡ ⋁ s' → (n : ℕ) → s n ≡ ⊤ → ∃[ m ∈ ℕ ] (s' m ≡ ⊤)
    ⋁-equal-implies-simulation {s} {s'} ⋁s≡⋁s' n sn≡⊤ = is-⊤-implies-contains-⊤ s' (sym (subst (λ z → z ≡ ⋁ s') (contains-⊤-implies-is-⊤ s (∣ n , sn≡⊤ ∣ )) ⋁s≡⋁s'))
-}


-- Idea from Tarmo Uustalu, Niccolò Veltri. Partiality and container monads:
-- It is enough to show that Sierpinski is closed under "Σ",
-- satisfying the two axioms below
ΣS : (x : Sierpinski) -> (x ≡ ⊤ -> Sierpinski) → Sierpinski
ΣS ⊤ v = v refl
ΣS ⊥ v = ⊥
ΣS (⋁ s) v = ⋁ λ n → ΣS (s n) (λ sn≡⊤ → v (contains-⊤-implies-is-⊤ s ∣ n , sn≡⊤ ∣))
-- these should be okay
ΣS (∨-comm {x} {y} i) v = {!!}
ΣS (∨-assoc i) v = {!!}
ΣS (∨-idem i) v = {!!}
ΣS (∨-bot i) v = {!!}
ΣS (∨-absorb n i) v = {!!}
ΣS (∨-distr i) v = {!!}
ΣS (isSetSierpinski x x₁ x₂ y i i₁) v = {!!}


ΣS-meaning-fst : (x : Sierpinski) (v : x ≡ ⊤ → Sierpinski) → ΣS x v ≡ ⊤ → x ≡ ⊤
ΣS-meaning-fst = Sierpinski-ind (λ x → (v : x ≡ ⊤ → Sierpinski) → ΣS x v ≡ ⊤ → x ≡ ⊤) (λ x → isPropΠ (λ v → isProp→ (isSetSierpinski _ _)))
                      (λ v eq → refl)
                      (λ v eq → eq)
                      λ {s} ih v eq → contains-⊤-implies-is-⊤ s (∥-∥rec isPropPropTrunc (λ { (n , p) → ∣ n , ih n _ p ∣ }) (is-⊤-implies-contains-⊤ _ eq))

ΣS-meaning-snd : (x : Sierpinski) (v : x ≡ ⊤ → Sierpinski) → (eq : ΣS x v ≡ ⊤) → v (ΣS-meaning-fst x v eq) ≡ ⊤
ΣS-meaning-snd = Sierpinski-ind (λ x → (v : x ≡ ⊤ → Sierpinski) → (eq : ΣS x v ≡ ⊤) → v (ΣS-meaning-fst x v eq) ≡ ⊤) (λ x → isPropΠ (λ v → isPropΠ (λ eq → isSetSierpinski _ _)))
                      (λ v eq → eq )
                      (λ v eq → Empty.rec (⊥≠⊤ eq))
                      λ {s} ih v eq → ∥-∥rec (isSetSierpinski _ _) (λ { (n , p) → cong v (isSetSierpinski (⋁ s) ⊤ _ _) ∙ ih n _ p }) (is-⊤-implies-contains-⊤ _ eq)

ΣS-meaning : (x : Sierpinski) (v : x ≡ ⊤ → Sierpinski) →
             (ΣS x v ≡ ⊤) → (Σ[ p ∈ x ≡ ⊤ ] (v p ≡ ⊤))
ΣS-meaning x v = (λ eq → (ΣS-meaning-fst x v eq) , (ΣS-meaning-snd x v eq))


ΣS-trivial-second : ∀ x → x ≡ ΣS x (λ _ → ⊤)
ΣS-trivial-second = Sierpinski-ind (λ x → x ≡ ΣS x (λ _ → ⊤)) (λ x → isSetSierpinski x (ΣS x (λ _ → ⊤)))
                      refl
                      refl
                      (λ {s} ih → cong ⋁ (funExt (λ n → ih n)))

ΣS-interchange : ∀ x y → ΣS x (λ _ → y) ≡ ΣS y (λ _ → x)
ΣS-interchange = Sierpinski-ind (λ x → ∀ y → ΣS x (λ _ → y) ≡ ΣS y (λ _ → x)) (λ x → isPropΠ (λ y → isSetSierpinski _ _))
                   (λ y → ΣS-trivial-second y)
                   (Sierpinski-ind (λ y →  ⊥ ≡ ΣS y (λ _ → ⊥)) (λ x → isSetSierpinski _ _)
                      refl
                      refl
                      (λ {s} ih → sym (⋁-idem {⊥}) ∙ cong ⋁ (funExt (λ n → ih n))))
                   λ {s} ih → Sierpinski-ind (λ y → ⋁ (λ n → ΣS (s n) (λ sn≡⊤ → y)) ≡ ΣS y (λ _ → ⋁ s)) (λ x → isSetSierpinski _ _)
                               (cong ⋁ (funExt (λ n → sym (ΣS-trivial-second (s n)))))
                               (cong ⋁ (funExt (λ n → ih n ⊥)) ∙ ⋁-idem {⊥})
                               (λ {s'} ih' → cong ⋁ (funExt (λ n → (ih n (⋁ s') ∙ {!!}) ∙ ih' n))) -- can we fill this hole?



eqSierpinski' : (x y : Sierpinski) -> (x ≡ ⊤ → y ≡ ⊤) → (y ≡ ⊤ → x ≡ ⊤) → x ≡ y
eqSierpinski' x y p q = ΣS-trivial-second x ∙ cong (ΣS x) (funExt (λ z → sym (p z))) ∙ ΣS-interchange x y ∙ cong (ΣS y) (funExt q) ∙ sym (ΣS-trivial-second y)

{-
eqSierpinski : (x y : Sierpinski) -> (x ≡ ⊤ → y ≡ ⊤) → (y ≡ ⊤ → x ≡ ⊤) → x ≡ y
eqSierpinski x y p q = Sierpinski-ind (λ x → (∀ y → (x ≡ ⊤ → y ≡ ⊤) → (y ≡ ⊤ → x ≡ ⊤) → x ≡ y)) (λ x → isPropΠ (λ y → isProp→ (isProp→ (isSetSierpinski x y))))
                  (λ y p _ → sym (p refl))
                  (λ y _ q → sym (not⊤-is-⊥ y (λ x → ⊥≠⊤ (q x))))
                (λ {s} ih y p q → Sierpinski-ind (λ y → (⋁ s ≡ ⊤ → y ≡ ⊤) → (y ≡ ⊤ → ⋁ s ≡ ⊤) → ⋁ s ≡ y) (λ y → isProp→ (isProp→ (isSetSierpinski (⋁ s) y)))
                      (λ _ p → p refl)
                      (λ p _ → not⊤-is-⊥ (⋁ s) (λ x → ⊥≠⊤ (p x)))
                      (λ {s'} ih' p' q' → let
                        s-simulates-s' : (n : ℕ) → ∃[ m ∈ ℕ ] (s m ≡ s m ∨ s' n)
                        -- want to pick m given by q', but we don't have a proof that ⋁s'≡⊤ yet...
                        s-simulates-s' n = ∣ {!!} , subst (λ z → z ≡ z ∨ s' n) (sym (ih _ (⋁ s') (λ sn≡⊤ → p' (contains-⊤-implies-is-⊤ s ∣ _ , sn≡⊤ ∣)) λ ⋁s'≡⊤ → {!is-⊤-implies-contains-⊤ s (q' ⋁s'≡⊤)!})) (sym (∨-absorb n) ∙ ∨-comm) ∣

                        s'-simulates-s : (n : ℕ) → ∃[ m ∈ ℕ ] (s' m ≡ s' m ∨ s n)
                        s'-simulates-s n = ∣ {!!} , subst (λ z → z ≡ z ∨ s n) (ih' _  (λ ⋁s≡⊤ → {!is-⊤-implies-contains-⊤ s' (p' ⋁s≡⊤)!}) (λ s'k≡⊤ → q' (contains-⊤-implies-is-⊤ s' ∣ _ , s'k≡⊤ ∣))) (sym (∨-absorb n) ∙ ∨-comm) ∣
                        in bisimilar-implies-⋁-equal s-simulates-s' s'-simulates-s)
                        y p q)
                   x y p q
-}

-- Another attempt to prove the above lemma `eqSierpinski`. I don't think this gets anywhere, but for now, I keep it just in case.
≡⊤-is-embedding : (x y : Sierpinski) → (x ≡ ⊤ ↔ y ≡ ⊤) → x ≡ y
≡⊤-is-embedding x y x≡⊤↔y≡⊤ = x≡y
  where
  x≡⊥↔y≡⊥ = x ≡ ⊥ 
              ↔⟨ (λ x≡⊥ → subst (λ x → ¬ x ≡ ⊤) (sym x≡⊥) ⊥≠⊤) , (not⊤-is-⊥ x) ⟩ 
            ¬ (x ≡ ⊤) 
              ↔⟨ (λ ¬x≡⊤ y≡⊤ → ¬x≡⊤ (snd x≡⊤↔y≡⊤ y≡⊤)) , (λ ¬y≡⊤ x≡⊤ → ¬y≡⊤ (fst x≡⊤↔y≡⊤ x≡⊤)) ⟩ 
            ¬ (y ≡ ⊤)
              ↔⟨ not⊤-is-⊥ y , (λ y≡⊥ → subst (λ y → ¬ y ≡ ⊤) (sym y≡⊥) ⊥≠⊤) ⟩
            y ≡ ⊥
              ↔∎
  x≡s↔y≡s : ∀ s → (x ≡ s) ↔ (y ≡ s)
  x≡s↔y≡s = 
    Sierpinski-ind
      (λ s → x ≡ s ↔ y ≡ s)
      (λ s → isProp× (isPropΠ λ _ → isSetSierpinski y s) (isPropΠ λ _ → isSetSierpinski x s))
      x≡⊤↔y≡⊤
      x≡⊥↔y≡⊥
      λ {f} → {!!}
  x≡y = sym (fst (x≡s↔y≡s x) refl)



isPropSemiDec : ∀ {ℓ}{P : Type ℓ} → isProp P → isProp (SemiDec P)
isPropSemiDec {P = P} isProp⟨P⟩ (x , p↔x≡⊤) (y , p↔y≡⊤) = eqSemiDec isProp⟨P⟩ (eqSierpinski' x y x≡⊤ y≡⊤)
  where
  x≡⊤ : x ≡ ⊤ → y ≡ ⊤
  x≡⊤ = λ x≡⊤ → fst p↔y≡⊤ (snd p↔x≡⊤ x≡⊤)
  y≡⊤ : y ≡ ⊤ → x ≡ ⊤
  y≡⊤ = λ y≡⊤ → fst p↔x≡⊤ (snd p↔y≡⊤ y≡⊤)


SemiDecOr : ∀ {ℓ} {ℓ'} {P : Type ℓ}{Q : Type ℓ'} → SemiDec P → SemiDec Q → SemiDec (∥ P ⊎ Q ∥)
SemiDecOr (sp , lp , rp) (sq , lq , rq) = (sp ∨ sq ,
                                           (∥-∥rec (isSetSierpinski _ _) (λ { (inl p) → contains-⊤-implies-is-⊤ _ ∣ 0 , lp p ∣ ; (inr q) → contains-⊤-implies-is-⊤ _ ∣ 1 , lq q ∣ })) ,
                                           λ w → ∥-∥rec isPropPropTrunc (λ { (zero , x) → ∣ inl (rp x) ∣ ; (suc n , x) → ∣ inr (rq x) ∣ }) (is-⊤-implies-contains-⊤ _ w))

{-
General notes.

Given a pointed type U , u₀ : U.

We probably want the properties:
  Bool -> U is injective
  U -> Prop is injective

Then we can define:

Partial A = Σ[ x ∈ U ] (x ≡ u₀) → A

if (U , u₀) := (Bool , true)
  -> Partial A = Maybe A

if (U , u₀) := (Sierpinski , ⊤)
  -> Partial A are the partial elements of A where it's semidecidable whether an element comes from A

if (U , u₀) := (Prop , Unit)
  -> Partial A are the partial elements of A

For a proposition (type) P, we can then define:

U-Dec P = Σ[ x ∈ U ] P ↔ x ≡ u₀

Expectation: isProp (U-Dec P).

Under which conditions can this be proved?
-}


{-
-- # A test/question: Can one show that "inserting ⊥'s" doesn't change the join?

open import Cubical.Data.Bool

isEven : ℕ → Bool
isEven zero = true
isEven (suc n) = not (isEven n)

div2 : ℕ → ℕ
div2 zero = zero
div2 (suc n) = if isEven n then suc (div2 n) else div2 n

insert-⊥ : (ℕ → Sierpinski) → (ℕ → Sierpinski)
insert-⊥ f n = if isEven n then f (div2 n) else ⊥

join-with-⊥s : (f : ℕ → Sierpinski) → ⋁ f ≡ ⋁ (insert-⊥ f)
join-with-⊥s f = bisimilar-implies-⋁-equal p q
  where
  p : (n : ℕ) → ∃[ m ∈ ℕ ] (f m ≡ (f m ∨ insert-⊥ f n))
  p zero = ∣ 0 , sym ∨-idem ∣
  p (suc n) with isEven n
  ... | false = ∣ div2 n , sym ∨-idem ∣
  ... | true = ∣ n , sym ∨-bot ∣
  q : (n : ℕ) → ∃[ m ∈ ℕ ] (insert-⊥ f m ≡ (insert-⊥ f m ∨ f n))
  q zero = ∣ 0 , sym ∨-idem ∣
  q (suc n) = {!!} --and so on
-}
