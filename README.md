## Constructive Notions of Ordinals in Homotopy Type Theory

by Tom de Jong, Nicolai Kraus, Fredrik Nordvall-Forsberg, Chuangjie Xu

This repository contains implementations of ordinals in Cubical Agda.
It implements the papers

* Connecting Constructive Notions of Ordinals in Homotopy Type Theory
  Nicolai Kraus, Fredrik Nordvall Forsberg, and Chuangjie Xu
  (DOI: [10.4230/LIPIcs.MFCS.2021.70](https://doi.org/10.4230/LIPIcs.MFCS.2021.70); root file MFCS2021/index.agda)

* Type-Theoretic Approaches to Ordinals
  Nicolai Kraus, Fredrik Nordvall Forsberg, and Chuangjie Xu
  (DOI: [10.1016/j.tcs.2023.113843](https://doi.org/10.1016/j.tcs.2023.113843); preprint: [arXiv:2208.03844](https://arxiv.org/abs/2208.03844); root file index.agda)

* Set-Theoretic and Type-Theoretic Ordinals Coincide
  Tom de Jong, Nicolai Kraus, Fredrik Nordvall Forsberg, and Chuangjie Xu
  (DOI: [10.1109/LICS56636.2023.10175762](https://doi.org/10.1109/LICS56636.2023.10175762); preprint: [arXiv:2301.10696](https://arxiv.org/abs/2301.10696); root file MEWO/index.agda)

The file Everything.agda imports all parts of the library.

The files are tested with

- Agda version 2.7.0.1

- Cubical Agda library
     (commit: e8ff0c1e18fd200d0c6a2d70f07811134df7d3cf)

- Agda standard library version 2.2 (for benchmarking Cantor Normal Form code)

The tag `Agda-v2.6.2.2-compatible` works with Agda version 2.6.2.2, agda/cubical v0.4 and standard library 1.7.1.
