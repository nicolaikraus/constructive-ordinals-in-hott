----------------------------------------------------------------------------------------------------
-- Logarithm of Cantor Normal Forms
----------------------------------------------------------------------------------------------------

{-# OPTIONS --cubical --safe #-}

module CantorNormalForm.Logarithm where

open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Function
open import Cubical.Data.Sigma
open import Cubical.Data.Sum
open import Cubical.Data.Empty
  renaming (rec to ⊥-rec)
open import Cubical.Relation.Nullary

open import CantorNormalForm.Base
open import CantorNormalForm.Wellfoundedness
open import CantorNormalForm.Addition
open import CantorNormalForm.Subtraction
open import CantorNormalForm.Multiplication
open import CantorNormalForm.Division
open import CantorNormalForm.Exponentiation

{-

Idea:

To compute the logarithm of  a = ω^ a₁ + a'  with base b ≥ ω,
we can compute x, y and u, v, w such that

  ω^ a₁ ≡ b ^ x · y        -- using the division theorem on a₁

     a' ≡ b ^ u · v + w    -- using induction hypothesis

To derive i, j , k from them such that

  ω^ a₁ + a' ≡ b ^ i · j + k

we may consider the following cases:

 ■ u < x -- take i = x, j = y, k = a'

 ■ u ≡ v -- two cases:

   □ y + v < b -- i = x, j = y + v, k = w

   □ y + v ≥ b -- i = x + 1, j = 1, k = b ^ x · (b - y - v) + w

 ■ u > x -- is it possible (we know left a' ≤ a₁ and b₁ < a₁)?

-}


-- Move it the division file. But it may not be needed.
quotient<divident : ∀ a b → (b>𝟎 : b > 𝟎) → b > 𝟏 → a > 𝟎
    → left a ≡ left b → a / b [ b>𝟎 ] < a
quotient<divident a@(ω^ a₁ + a' [ _ ])
                  b@(ω^ b₁ + b' [ _ ])
                  b>𝟎 b>𝟏 <₁ a₁≡b₁ = ≰→> c≱a
 where
  thm = Thm[div] a b b>𝟎
  c d : Cnf
  c = a / b [ b>𝟎 ]
  d = fst (snd thm)
  a≡b·c+d : a ≡ b · c + d
  a≡b·c+d = fst (snd (snd thm))
  a<b·a : a < b · a
  a<b·a = <l· b a b>𝟏 <₁ (inr (sym a₁≡b₁))
  c≱a : c ≥ a → ⊥
  c≱a c≥a = <-irreflexive a≡b·c+d a<b·c+d
   where
    b·a≤b·c : b · a ≤ b · c
    b·a≤b·c = ·r-is-≤-monotone b a c c≥a
    a<b·c : a < b · c
    a<b·c = <∘≤-in-< a<b·a b·a≤b·c
    a<b·c+d : a < b · c + d
    a<b·c+d = <∘≤-in-< a<b·c (≤+r (b · c) d)

private
 P : Cnf → Type
 P a = ∀ b → a > 𝟎 → b > 𝟏
     → Σ[ x ∈ Cnf ] Σ[ y ∈ Cnf ] Σ[ z ∈ Cnf ]
          (a ≡ b ^ x · y + z) × (𝟎 < y) × (y < b) × (z < b ^ x)

 log-step : ∀ a → (∀ x → x < a → P x) → P a
 log-step a h b a>𝟎 b>𝟏 with <-tri a b
 log-step a h b a>𝟎 b>𝟏 | inl a<b = 𝟎 , a , 𝟎 , a≡b^𝟎·a+𝟎 , a>𝟎 , a<b , <₁
  where
   a≡b^𝟎·a+𝟎 : a ≡ b ^ 𝟎 · a + 𝟎
   a≡b^𝟎·a+𝟎 =
      a
     ≡⟨ sym (𝟏·-is-identity a) ⟩
      𝟏 · a
     ≡⟨ sym +𝟎-is-identity ⟩
      b ^ 𝟎 · a + 𝟎 ∎
 log-step a h b a>𝟎 b>𝟏 | inr (inr a≡b) = 𝟏 , 𝟏 , 𝟎 , a≡b^𝟏·𝟏+𝟎 , <₁ , b>𝟏 , b^𝟏>𝟎
  where
   b>𝟎 : b > 𝟎
   b>𝟎 = ≤∘<-in-< 𝟎≤ b>𝟏
   a≡b^𝟏·𝟏+𝟎 : a ≡ b ^ 𝟏 · 𝟏 + 𝟎
   a≡b^𝟏·𝟏+𝟎 =
      a
     ≡⟨ a≡b ⟩
      b
     ≡⟨ sym (nz^𝟏-is-identity' b>𝟎 ) ⟩
      b ^ 𝟏
     ≡⟨ sym (·𝟏-is-identity _) ⟩
      b ^ 𝟏 · 𝟏
     ≡⟨ sym +𝟎-is-identity ⟩
      b ^ 𝟏 · 𝟏 + 𝟎 ∎
   b^𝟏>𝟎 : b ^ 𝟏 > 𝟎
   b^𝟏>𝟎 = nz^>𝟎 𝟏 b>𝟎
 log-step a@(ω^ a₁@(ω^ _ + _ [ _ ]) + a' [ _ ]) h
          b@(ω^ 𝟎 + b' [ _ ]) <₁ b>𝟏 | inr (inl (<₂ <₁)) = {!!}
 -- b₁ ≡ 𝟎 & b₁ < a₁  --- base < ω & log ≥ ω  -- EQ 5
 log-step a@(ω^ a₁@(ω^ _ + _ [ _ ]) + a' [ _ ]) h
          b@(ω^ b₁@(ω^ _ + _ [ _ ]) + b' [ _ ]) <₁ b>𝟏 | inr (inl (<₂ b₁<a₁)) = {!!}
 -- b₁ > 𝟎 & b₁ < a₁  --- base ≥ ω  -- EQ 6 & 7
    -- case 1 : a / b < a -- log < ω -- EQ 6
    -- case 2 : a / b ≡ a -- log ≥ ω -- EQ 7 -- b · a ≡ a
 log-step a@(ω^ a₁ + a' [ _ ]) h
          b@(ω^ b₁ + b' [ _ ]) <₁ b>𝟏 | inr (inl (<₃ b₁≡a₁ b'<a')) =
 -- a₁ ≡ b₁  --- base < ω & log < ω  -- EQ 4  -- combine it with EQ 6
   x' , y , z' , a≡b^x'·y+z' , 𝟎<y , y<b , z'<b^x'
  where
   b<a : b < a
   b<a = <₃ b₁≡a₁ b'<a'
   b>𝟎 : b > 𝟎
   b>𝟎 = ≤∘<-in-< 𝟎≤ b>𝟏
   c d : Cnf
   c = a / b [ b>𝟎 ]
   d = fst (snd (Thm[div] a b b>𝟎))
   a≡b·c+d : a ≡ b · c + d
   a≡b·c+d = fst (snd (snd (Thm[div] a b b>𝟎)))
   d<b : d < b
   d<b = snd (snd (snd (Thm[div] a b b>𝟎)))
   c>𝟎 : c > 𝟎
   c>𝟎 = dividend≥divisor→quotient>𝟎 a b b>𝟎 (inl b<a)
   c<a : c < a
   c<a = quotient<divident a b b>𝟎 b>𝟏 <₁ (sym b₁≡a₁)
   IH : Σ[ x ∈ Cnf ] Σ[ y ∈ Cnf ] Σ[ z ∈ Cnf ]
          (c ≡ b ^ x · y + z) × (𝟎 < y) × (y < b) × (z < b ^ x)
   IH = h c c<a b c>𝟎 b>𝟏
   x y z : Cnf
   x = fst IH
   y = fst (snd IH)
   z = fst (snd (snd IH))
   c≡b^x·y+z : c ≡ b ^ x · y + z
   c≡b^x·y+z = fst (snd (snd (snd IH)))
   𝟎<y : 𝟎 < y
   𝟎<y = fst (snd (snd (snd (snd IH))))
   y<b : y < b
   y<b = fst (snd (snd (snd (snd (snd IH)))))
   z<b^x : z < b ^ x
   z<b^x = snd (snd (snd (snd (snd (snd IH)))))
   x' z' : Cnf
   x' = 𝟏 + x
   z' = b · z + d
   b^⟨𝟏+x⟩≡b·b^x : b ^ (𝟏 + x) ≡ b · b ^ x
   b^⟨𝟏+x⟩≡b·b^x = exponent-sum b 𝟏 x -- : b ^ (𝟏 + x) ≡ b ^ 𝟏 · b ^ x
                 ∙ cong (_· b ^ x) (nz^𝟏-is-identity' b>𝟎)
   a≡b^x'·y+z' : a ≡ b ^ (𝟏 + x) · y + (b · z + d)
   a≡b^x'·y+z' =
      a
     ≡⟨ a≡b·c+d ⟩
      b · c + d
     ≡⟨ cong (λ w → b · w + d) c≡b^x·y+z ⟩
      b · (b ^ x · y + z) + d
     ≡⟨ cong (_+ d) (·-is-left-distributive b (b ^ x · y) z) ⟩
      b · (b ^ x · y) + b · z + d
     ≡⟨ sym (+-is-assoc (b · (b ^ x · y)) (b · z) d) ⟩
      b · (b ^ x · y) + (b · z + d)
     ≡⟨ cong (_+ (b · z + d)) (·-is-assoc b (b ^ x) y) ⟩
      (b · b ^ x) · y + (b · z + d)
     ≡⟨ cong (λ u → u · y + (b · z + d)) (sym b^⟨𝟏+x⟩≡b·b^x) ⟩
      b ^ (𝟏 + x) · y + (b · z + d) ∎
   z+𝟏≤b^x : z + 𝟏 ≤ b ^ x
   z+𝟏≤b^x = <→+𝟏≤ z<b^x
   b·⟨z+𝟏⟩≤b·b^x : b · (z + 𝟏) ≤ b · b ^ x
   b·⟨z+𝟏⟩≤b·b^x = ·r-is-≤-monotone b (z + 𝟏) (b ^ x) z+𝟏≤b^x
   b·⟨z+𝟏⟩≡b·z+b : b · (z + 𝟏) ≡ b · z + b
   b·⟨z+𝟏⟩≡b·z+b = ·-is-left-distributive b z 𝟏
   z'<b·z+b : b · z + d < b · z + b
   z'<b·z+b = +r-is-<-monotone (b · z) d b d<b
   z'<b·⟨z+𝟏⟩ : b · z + d < b · (z + 𝟏)
   z'<b·⟨z+𝟏⟩ = <∘≤-in-< z'<b·z+b (inr b·⟨z+𝟏⟩≡b·z+b)
   z'<b·b^x : b · z + d < b · b ^ x
   z'<b·b^x = <∘≤-in-< z'<b·⟨z+𝟏⟩ b·⟨z+𝟏⟩≤b·b^x
   z'<b^x' : b · z + d < b ^ (𝟏 + x)
   z'<b^x' = <∘≤-in-< z'<b·b^x (inr b^⟨𝟏+x⟩≡b·b^x)


{-
-- Commenting out the following to speed up the loading


-- TODO: Remove the condition a > 𝟎
Thm[log] : (a b : Cnf) → a > 𝟎 → b > 𝟏
         → Σ[ x ∈ Cnf ] Σ[ y ∈ Cnf ] Σ[ z ∈ Cnf ]
              (a ≡ b ^ x · y + z)
            × (𝟎 < y) × (y < b)
            × (z < b ^ x)
Thm[log] = wf-ind log-step


log : (a b : Cnf) → a > 𝟎 → b > 𝟏 → Cnf
log a b r s = fst (Thm[log] a b r s)

log-spec : ∀ a b → (a>𝟎 : a > 𝟎) (b>𝟏 : b > 𝟏)
         → b ^ (log a b a>𝟎 b>𝟏) ≤ a
log-spec a b a>𝟎 b>𝟏 = goal
 where
  thm = Thm[log] a b a>𝟎 b>𝟏
  x y z : Cnf
  x = fst thm
  y = fst (snd thm)
  z = fst (snd (snd thm))
  a≡b^x·y+z : a ≡ b ^ x · y + z
  a≡b^x·y+z = fst (snd (snd (snd thm)))
  y>𝟎 : y > 𝟎
  y>𝟎 = fst (snd (snd (snd (snd thm))))
  b^x·y≤b^x·y+z : b ^ x · y ≤ b ^ x · y + z
  b^x·y≤b^x·y+z = ≤+r (b ^ x · y) z
  b^x≤b^x·y : b ^ x ≤ b ^ x · y
  b^x≤b^x·y = ≤·r (b ^ x) y y>𝟎
  goal : b ^ x ≤ a
  goal = ≤-trans b^x≤b^x·y (≤-trans b^x·y≤b^x·y+z (inr a≡b^x·y+z))

greatest-log : ∀ a b c → (a>𝟎 : a > 𝟎) (b>𝟏 : b > 𝟏)
             → b ^ c ≤ a → c ≤ log a b a>𝟎 b>𝟏
greatest-log a b c a>𝟎 b>𝟏 b^c≤a = ≮→≥ x≮c  -- : c ≤ x
 where
  b>𝟎 : b > 𝟎
  b>𝟎 = ≤∘<-in-< 𝟎≤ b>𝟏
  thm = Thm[log] a b a>𝟎 b>𝟏
  x y z : Cnf
  x = fst thm
  y = fst (snd thm)
  z = fst (snd (snd thm))
  a≡b^x·y+z : a ≡ b ^ x · y + z
  a≡b^x·y+z = fst (snd (snd (snd thm)))
  y<b : y < b
  y<b = fst (snd (snd (snd (snd (snd thm)))))
  z<b^x : z < b ^ x
  z<b^x = snd (snd (snd (snd (snd (snd thm)))))
  x≮c : x < c → ⊥
  x≮c x<c = ≤→≯ b^c≤a a<b^c
   where
    x+𝟏≤c : x + 𝟏 ≤ c
    x+𝟏≤c = <→+𝟏≤ x<c
    b^⟨x+𝟏⟩≤b^c : b ^ (x + 𝟏) ≤ b ^ c
    b^⟨x+𝟏⟩≤b^c = ^r-is-≤-monotone b (x + 𝟏) c b>𝟎 x+𝟏≤c
    b^⟨x+𝟏⟩≡b^x·b : b ^ (x + 𝟏) ≡ b ^ x · b
    b^⟨x+𝟏⟩≡b^x·b = ^+𝟏-spec b x
    y+𝟏≤b : y + 𝟏 ≤ b
    y+𝟏≤b = <→+𝟏≤ y<b
    b^x·⟨y+𝟏⟩≤b^x·b : b ^ x · (y + 𝟏) ≤ b ^ x · b
    b^x·⟨y+𝟏⟩≤b^x·b = ·r-is-≤-monotone (b ^ x) (y + 𝟏) b y+𝟏≤b
    b^x·⟨y+𝟏⟩≡b^x·y+b^x : b ^ x · (y + 𝟏) ≡ b ^ x · y + b ^ x
    b^x·⟨y+𝟏⟩≡b^x·y+b^x = ·+𝟏-spec (b ^ x) y
    b^x·y+z<b^x·y+b^x : b ^ x · y + z < b ^ x · y + b ^ x
    b^x·y+z<b^x·y+b^x = +r-is-<-monotone (b ^ x · y) z (b ^ x) z<b^x
    a<b^x·y+b^x : a < b ^ x · y + b ^ x
    a<b^x·y+b^x = ≤∘<-in-< (inr (sym a≡b^x·y+z)) b^x·y+z<b^x·y+b^x
    a<b^x·⟨y+𝟏⟩ : a < b ^ x · (y + 𝟏)
    a<b^x·⟨y+𝟏⟩ = <∘≤-in-< a<b^x·y+b^x (inr b^x·⟨y+𝟏⟩≡b^x·y+b^x)
    a<b^x·b : a < b ^ x · b
    a<b^x·b = <∘≤-in-< a<b^x·⟨y+𝟏⟩ b^x·⟨y+𝟏⟩≤b^x·b
    a<b^⟨x+𝟏⟩ : a < b ^ (x + 𝟏)
    a<b^⟨x+𝟏⟩ = <∘≤-in-< a<b^x·b (inr b^⟨x+𝟏⟩≡b^x·b)
    a<b^c : a < b ^ c
    a<b^c = <∘≤-in-< a<b^⟨x+𝟏⟩ b^⟨x+𝟏⟩≤b^c

-}
