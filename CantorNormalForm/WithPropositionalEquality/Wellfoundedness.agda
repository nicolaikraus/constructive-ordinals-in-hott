----------------------------------------------------------------------------------------------------
-- Wellfoundedness of Cantor Normal Forms
----------------------------------------------------------------------------------------------------

{-# OPTIONS --cubical-compatible #-}

module CantorNormalForm.WithPropositionalEquality.Wellfoundedness where

open import Induction.WellFounded
  renaming (WellFounded to isWellFounded;
            Acc to isAcc)

open import Agda.Builtin.Equality

open import CantorNormalForm.WithPropositionalEquality.Base


𝟎IsAcc : isAcc _<_ 𝟎
𝟎IsAcc = acc (λ ())

mutual

 Lm[fst-acc] : ∀ {a a'} → isAcc _<_ a' → a ≡ a'
   → ∀ {b x} → isAcc _<_ b → x < a' → (r : x ≥ left b)
   → isAcc _<_ (ω^ x + b [ r ])
 Lm[fst-acc] {a} {a'} (acc ξ) a=a' {b} {x} acᵇ x<a' r = acc goal
   where
    goal : ∀ {z} → z < ω^ x + b [ r ] → isAcc _<_ z
    goal  {𝟎} <₁ = 𝟎IsAcc
    goal {ω^ c + d [ s ]} (<₂ c<y) = Lm[fst-acc] (ξ x<a') refl IH c<y s
     where
      IH : isAcc _<_ d
      IH = goal (<-trans (right< c d s) (<₂ c<y))
    goal {ω^ c + d [ s ]} (<₃ c=y d<b) = Lm[snd-acc] (ξ x<a') c=y acᵇ d<b s

 Lm[snd-acc] : ∀ {a a'} → isAcc _<_ a' → a ≡ a'
   → ∀ {c y} → isAcc _<_ c → y < c → (r : a ≥ left y)
   → isAcc _<_ (ω^ a + y [ r ])
 Lm[snd-acc] {a} {.a} acᵃ refl {c} {y} (acc ξᶜ) y<c r = acc goal
  where
   goal : ∀ {z} → z < ω^ a + y [ r ] → isAcc _<_ z
   goal {𝟎} <₁ = 𝟎IsAcc
   goal {ω^ b + d [ t ]} (<₂ b<a) = Lm[fst-acc] acᵃ refl IH b<a t
    where
     IH : isAcc _<_ d
     IH = goal (<-trans (right< b d t) (<₂ b<a))
   goal {ω^ b + d [ t ]} (<₃ refl d<y) = Lm[snd-acc] acᵃ refl (ξᶜ y<c) d<y t

<-is-wellfounded : isWellFounded _<_
<-is-wellfounded 𝟎 = 𝟎IsAcc
<-is-wellfounded (ω^ a + b [ r ]) = acc goal
 where
  goal : ∀ {z} → z < ω^ a + b [ r ] → isAcc _<_ z
  goal {𝟎} <₁ = 𝟎IsAcc
  goal {ω^ c + d [ s ]} (<₂ c<a) =
    Lm[fst-acc] (<-is-wellfounded a) refl IH c<a s
   where
    IH : isAcc _<_ d
    IH = goal (<-trans (right< c d s) (<₂ c<a))
  goal {ω^ c + d [ s ]} (<₃ c=a d<b) =
    Lm[snd-acc] (<-is-wellfounded a) c=a (<-is-wellfounded b) d<b s

wf-ind : ∀ {ℓ''} {P : Cnf → Set ℓ''} →
         ((x : Cnf) → ((y : Cnf) → y < x → P y) → P x) → (x : Cnf) → P x
wf-ind {P = P} step = All.wfRec <-is-wellfounded _ P (λ x s → step x λ y → s)
