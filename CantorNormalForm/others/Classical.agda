{-# OPTIONS --cubical #-}

module CantorNormalForm.Classical where

open import Cubical.Foundations.Prelude
open import Cubical.Foundations.HLevels
open import Cubical.HITs.PropositionalTruncation
  renaming (rec to ∥-∥-rec)
open import Cubical.Data.Unit
  renaming (tt to ⋆)
open import Cubical.Data.Sum
open import Cubical.Data.Sigma
open import Cubical.Data.Empty
  renaming (rec to ⊥-rec)
open import Cubical.Data.Nat
open import Cubical.Relation.Nullary

open import General-Properties
open import CantorNormalForm.Base
open import CantorNormalForm.Compactification


--
-- If the supremum of any sequence of CNF's exists, then we can decide
-- whether a sequence is constantly zero.
--
has-sup-decides-constantly-zero :
  has-sup → (f : ℕ → CCNF) → Dec (∀ i → f i ≡ inl 𝟎)
has-sup-decides-constantly-zero (⊔ , w) f with ≺-tri (⊔ f) (inl 𝟎)
... | inl ⊔f<𝟎 = ⊥-rec (⊀𝟎 ⊔f<𝟎)
... | inr (inl ⊔f>𝟎) = no  (λ h → ≼→⊁ (snd (w f) (inl 𝟎) (λ i → inr (sym (h i)))) ⊔f>𝟎)
... | inr (inr ⊔f≡𝟎) = yes (λ i → ≼𝟎→≡𝟎 (subst (f i ≼_) ⊔f≡𝟎 (fst (w f) i)))


--
-- Assuming LEM, every nonempty subset of CNF's has a minimal element.
--
minimal-witness : LEM → (P : CCNF → Type) → (∀ x → isProp (P x))
 → ∀ x → P x → Σ[ a ∈ CCNF ] P a × (∀ z → P z → a ≼ z)
minimal-witness lem P pvP = CCNF-wf-ind step
 where
  A Q : CCNF → Type
  A x = ∀ z → P z → x ≼ z
  Q x = P x → Σ[ y ∈ CCNF ] P y × A y
  pvA : ∀ x → isProp (A x)
  pvA x = isPropΠ2 (λ _ _ → isProp⟨≼⟩)
  pvPA : ∀ x → isProp (P x × A x)
  pvPA x = isProp× (pvP x) (pvA x)
  ΣPA-isProp : isProp (Σ[ y ∈ CCNF ] P y × A y)
  ΣPA-isProp (y₀ , py₀ , ay₀) (y₁ , py₁ , ay₁) = Σ≡Prop pvPA y₀≡y₁
   where
    y₀≤y₁ : y₀ ≼ y₁
    y₀≤y₁ = ay₀ y₁ py₁
    y₁≤y₀ : y₁ ≼ y₀
    y₁≤y₀ = ay₁ y₀ py₀
    y₀≡y₁ : y₀ ≡ y₁
    y₀≡y₁ = ≼-antisym y₀≤y₁ y₁≤y₀
  step : ∀ x → (∀ y → y ≺ x → Q y) → Q x
  step x h px with lem (A x) (pvA x)
  ... | yes a = x , px , a
  ... | no ¬a = ∥-∥-rec ΣPA-isProp claim₁ claim₀
   where
    claim₀ : ∃[ z ∈ CCNF ] ¬ (P z → x ≼ z)
    claim₀ = LEM-¬∀-∃¬ _ (λ z → isPropΠ (λ _ → isProp⟨≼⟩)) lem ¬a
    claim₁ : (Σ[ z ∈ CCNF ] ¬ (P z → x ≼ z)) → Σ[ y ∈ CCNF ] P y × A y
    claim₁ (z , f) = goal
     where
      z<x : z ≺ x
      z<x with ≺-tri z x
      ... | inl z<x = z<x
      ... | inr z≥x = ⊥-rec (f (λ _ → z≥x))
      pz : P z
      pz with lem (P z) (pvP z)
      ... | yes p = p
      ... | no ¬p = ⊥-rec (f λ p → ⊥-rec (¬p p))
      goal : Σ[ y ∈ CCNF ] P y × A y
      goal = h z z<x pz


--
-- Therefore, assuming LEM, we can compute the supremen of any
-- sequence of CNF's.
--
LEM-computes-sup : LEM → (f : ℕ → CCNF) → Σ[ a ∈ CCNF ] a is-sup-of f
LEM-computes-sup lem f = minimal-witness lem P pvP (inr ⋆) (λ _ → ≼⋆)
 where
  P : CCNF → Type
  P x = ∀ i → f i ≼ x
  pvP : ∀ x → isProp (P x)
  pvP x = isPropΠ (λ y → isProp⟨≼⟩)
  claim : Σ[ a ∈ CCNF ] P a × (∀ z → P z → a ≼ z)
  claim = minimal-witness lem P pvP (inr ⋆) (λ _ → ≼⋆)

LEM-implies-has-sup : LEM → has-sup
LEM-implies-has-sup lem = ⊔ , ⊔-calc-sup
 where
  claim : (f : ℕ → CCNF) → Σ[ a ∈ CCNF ] a is-sup-of f
  claim = LEM-computes-sup lem
  ⊔ : (ℕ → CCNF) → CCNF
  ⊔ f = fst (claim f)
  ⊔-calc-sup : calc-sup ⊔
  ⊔-calc-sup f = snd (claim f)
