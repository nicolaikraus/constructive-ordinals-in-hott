{-# OPTIONS --cubical #-}

module CantorNormalForm.Compactification where

open import Cubical.Foundations.Prelude
open import Cubical.Foundations.HLevels
open import Cubical.HITs.PropositionalTruncation
  renaming (rec to ∥-∥-rec)
open import Cubical.Data.Unit
  renaming (tt to ⋆)
open import Cubical.Data.Sum
open import Cubical.Data.Sigma
open import Cubical.Data.Empty
  renaming (rec to ⊥-rec)
open import Cubical.Data.Nat hiding (_+_; _·_)
open import Cubical.Relation.Nullary
open import Cubical.Relation.Binary
open BinaryRelation
open import Cubical.Induction.WellFounded
  renaming (WellFounded to isWellFounded;
            Acc to isAcc)
open import Cubical.Foundations.Function

open import General-Properties
open import CantorNormalForm.Base
open import CantorNormalForm.WellFoundedness


CCNF : Type
CCNF = CNF ⊎ Unit

toCNF : CCNF → CNF
toCNF (inl a) = a
toCNF (inr ⋆) = 𝟎

data _≺_ : CCNF → CCNF → Type where
 l≺l : {a b : CNF} → a < b → inl a ≺ inl b
 l≺r : {a : CNF} → inl a ≺ inr ⋆

_≻_ _≽_ _≼_ : CCNF → CCNF → Type
a ≻ b = b ≺ a
a ≽ b = (a ≻ b) ⊎ (a ≡ b)
a ≼ b = b ≽ a

l≼l : {a b : CNF} → a ≤ b → inl a ≼ inl b
l≼l (inl r) = inl (l≺l r)
l≼l (inr e) = inr (cong inl e)

l≺l⁻¹ : {a b : CNF} → inl a ≺ inl b → a < b
l≺l⁻¹ (l≺l a<b) = a<b

l≼l⁻¹ : {a b : CNF} → inl a ≼ inl b → a ≤ b
l≼l⁻¹ (inl r) = inl (l≺l⁻¹ r)
l≼l⁻¹ (inr e) = inr (cong toCNF e)

≼⋆ : {x : CCNF} → x ≼ inr ⋆
≼⋆ {inl a} = inl l≺r
≼⋆ {inr ⋆} = inr refl

≺-irrefl : {x : CCNF} → ¬ (x ≺ x)
≺-irrefl {inl a} (l≺l a<a) = <-irrefl a<a

≺-irreflexive : ∀ {x y} → x ≡ y → ¬ x ≺ y
≺-irreflexive e = subst (λ x → ¬ _ ≺ x) e ≺-irrefl

≺-trans : ∀ {x y z} → x ≺ y → y ≺ z → x ≺ z
≺-trans (l≺l r) (l≺l s) = l≺l (<-trans r s)
≺-trans (l≺l r) l≺r = l≺r

≺→⊁ : ∀ {x y} → x ≺ y → ¬ (x ≻ y)
≺→⊁ (l≺l r) (l≺l s) = <→≯ r s

≼-refl : ∀ {x} → x ≼ x
≼-refl = inr refl

≼-trans : ∀ {x y z} → x ≼ y → y ≼ z → x ≼ z
≼-trans (inl x≺y) (inl y≺z) = inl (≺-trans x≺y y≺z)
≼-trans (inl x≺y) (inr z≡y) = inl (subst (_ ≺_) (sym z≡y) x≺y)
≼-trans (inr y≡x) (inl y≺z) = inl (subst (_≺ _) y≡x y≺z)
≼-trans (inr y≡x) (inr z≡y) = inr (z≡y ∙ y≡x)

≼-antisym : ∀ {a b} → a ≼ b → b ≼ a → a ≡ b
≼-antisym (inl a≺b) (inl a≻b) = ⊥-rec (≺→⊁ a≺b a≻b)
≼-antisym (inl a≺b) (inr a≡b) = ⊥-rec (≺-irreflexive a≡b a≺b)
≼-antisym (inr b≡a) (inl a≻b) = ⊥-rec (≺-irreflexive b≡a a≻b)
≼-antisym (inr b≡a) (inr a≡b) = a≡b

≺-in-≼ : ∀ {x y} → x ≺ y → x ≼ y
≺-in-≼ = inl

≺∘≼-in-≺ : ∀ {a b c} → a ≺ b → b ≼ c → a ≺ c
≺∘≼-in-≺ a≺b (inl b≺c) = ≺-trans a≺b b≺c
≺∘≼-in-≺ a≺b (inr c≡b) = subst (_ ≺_) (sym c≡b) a≺b

≼∘≺-in-≺ : ∀ {a b c} → a ≼ b → b ≺ c → a ≺ c
≼∘≺-in-≺ (inl a≺b) b≺c = ≺-trans a≺b b≺c
≼∘≺-in-≺ (inr b≡a) b≺c = subst (_≺ _) b≡a b≺c

≼→⊁ : ∀ {a b} → a ≼ b → ¬ (a ≻ b)
≼→⊁ (inl a<b) = ≺→⊁ a<b
≼→⊁ (inr b≡a) = ≺-irreflexive b≡a

⊀𝟎 : ∀ {x} → ¬ (x ≺ inl 𝟎)
⊀𝟎 (l≺l ())

≼𝟎→≡𝟎 : ∀ {x} → x ≼ inl 𝟎 → x ≡ inl 𝟎
≼𝟎→≡𝟎 (inl x≺𝟎) = ⊥-rec (⊀𝟎 x≺𝟎)
≼𝟎→≡𝟎 (inr 𝟎≡x) = sym 𝟎≡x

≺-tri : ∀ x y → (x ≺ y) ⊎ (x ≽ y)
≺-tri (inl a) (inl b) with <-tri a b
... | inl a<b = inl (l≺l a<b)
... | inr a≥b = inr (l≼l a≥b)
≺-tri (inl a) (inr ⋆) = inl l≺r
≺-tri (inr ⋆) (inl b) = inr (inl l≺r)
≺-tri (inr ⋆) (inr ⋆) = inr (inr refl)


l-is-acc : (a : CNF) → isAcc _≺_ (inl a)
l-is-acc = wf-ind step
 where
  step : ∀ x → (∀ y → y < x → isAcc _≺_ (inl y)) → isAcc _≺_  (inl x)
  step x p = acc q
   where
    q : ∀ y → y ≺ inl x → isAcc _≺_ y
    q (inl y) (l≺l y<x) = p y y<x

≺IsWellFounded : isWellFounded _≺_
≺IsWellFounded (inl a) = l-is-acc a
≺IsWellFounded (inr ⋆) = acc h
 where
  h : ∀ x → x ≺ inr ⋆ → isAcc _≺_ x
  h (inl a) l≺r = l-is-acc a

open WFI ≺IsWellFounded public
 renaming (induction to CCNF-wf-ind)

CCNF-is-set : isSet CCNF
CCNF-is-set = isSetSum CNF-is-set isSetUnit

isProp⟨≺⟩ : ∀ {x y} → isProp (x ≺ y)
isProp⟨≺⟩ (l≺l a<b) (l≺l a<b') = cong l≺l (isProp⟨<⟩ a<b a<b')
isProp⟨≺⟩  l≺r       l≺r       = refl

isProp⟨≼⟩ : ∀ {x y} → isProp (x ≼ y)
isProp⟨≼⟩ (inl x≺y) (inl x≺y') = cong inl (isProp⟨≺⟩ x≺y x≺y')
isProp⟨≼⟩ (inl x≺y) (inr y≡x') = ⊥-rec (≺-irreflexive (sym y≡x') x≺y)
isProp⟨≼⟩ (inr y≡x) (inl x≺y') = ⊥-rec (≺-irreflexive (sym y≡x) x≺y')
isProp⟨≼⟩ (inr y≡x) (inr y≡x') = cong inr (CCNF-is-set _ _ y≡x y≡x')

open import Abstract.ZerSucLim _≺_ _≼_ public
open Properties
 CCNF-is-set
 (λ _ _ → isProp⟨≺⟩)
 (λ _ _ → isProp⟨≼⟩)
 (λ _ → ≺-irrefl)
-- (λ _ _ _ → ≺-trans)
 (λ _ → ≼-refl)
-- (λ _ _ _ → ≼-trans)
 ≼-antisym
-- ≺-in-≼
 ≺∘≼-in-≺
 public

open import CantorNormalForm.Classifiability
 using (CNF-is-Σclassifiable)

CCNF-is-Σclassifiable : ∀ x → is-Σclassifiable x
CCNF-is-Σclassifiable (inl a) with CNF-is-Σclassifiable a
... | inl a-is-zero = inl goal
 where
  goal : is-zero (inl a)
  goal (inl b) = l≼l (a-is-zero b)
  goal (inr ⋆) = inl l≺r
... | inr (inl (b , (b<a , p) , q)) = inr (inl (inl b , goal))
 where
  p' : ∀ x → inl b ≺ x → inl a ≼ x
  p' (inl c) (l≺l b<c) = l≼l (p c b<c)
  p' (inr ⋆) l≺r = inl l≺r
  q' : ∀ x → x ≺ inl a → x ≼ inl b
  q' (inl c) (l≺l c<a) = l≼l (q c c<a)
  q' (inr ⋆) ()
  goal : (inl a) is-strong-suc-of (inl b)
  goal = (l≺l b<a , p') , q'
... | inr (inr (f , (_ , p) , q)) = inr (inr (inl ∘ f , goal))
 where
  p' : ∀ x → (∀ i → inl (f i) ≼ x) → inl a ≼ x
  p' (inl c) h = l≼l (p c (l≼l⁻¹ ∘ h))
  p' (inr ⋆) h = inl l≺r
  goal : (inl a) is-lim-of (inl ∘ f)
  goal = ((λ i → inl (l≺l q)) , p') , l≺l q
CCNF-is-Σclassifiable (inr ⋆) = inr (inr (inl ∘ f , ⋆-is-lim-of-f))
 where
  f : ℕ → CNF
  f  0      = 𝟎
  f (suc i) = ω^⟨ f i ⟩
  fact : (a : CNF) → Σ[ n ∈ ℕ ] a < f n
  fact 𝟎 = 1 , <₁
  fact ω^ a + b [ r ] = suc (fst (fact a)) , <₂ (snd (fact a))
  sup-of-f-is-⋆ : ∀ x → (∀ i → inl (f i) ≼ x) → x ≡ inr ⋆
  sup-of-f-is-⋆ (inl a) h = ⊥-rec (≼→⊁ (h n) (l≺l a<fn))
   where
    n : ℕ
    n = fst (fact a)
    a<fn : a < f n
    a<fn = snd (fact a)
  sup-of-f-is-⋆ (inr ⋆) h = refl
  ⋆-is-lim-of-f : (inr ⋆) is-lim-of (inl ∘ f)
  ⋆-is-lim-of-f = ((λ i → inl l≺r) , λ x h → inr (sup-of-f-is-⋆ x h)) , l≺r


CCNF-is-classifiable : ∀ x → is-classifiable x
CCNF-is-classifiable = to-is-classifiable ∘ CCNF-is-Σclassifiable


open ClassifiabilityInduction CCNF-is-classifiable ≺IsWellFounded public
