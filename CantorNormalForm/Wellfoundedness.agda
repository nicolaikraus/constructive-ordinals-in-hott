----------------------------------------------------------------------------------------------------
-- Wellfoundedness of Cantor Normal Forms
----------------------------------------------------------------------------------------------------

{- We recall the wellfoundedness proof of CNF's from

     Fredrik Nordvall Forsberg, Chuangjie Xu, and Neil Ghani.
     Threee quivalent ordinal notation systems in cubical agda.
     In Proceedings of the 9th ACM SIGPLAN International Conference
     on Certified Programs and Proofs, CPP 2020, page 172–185.
     Association for Computing Machinery, 2020.
-}

{-# OPTIONS --cubical --erasure --safe #-}

module CantorNormalForm.Wellfoundedness where

open import Cubical.Foundations.Prelude
open import Cubical.Data.Sum
open import Cubical.Data.Sigma
open import Cubical.Data.Empty
  renaming (rec to ⊥-rec)
open import Cubical.Relation.Nullary
open import Cubical.Relation.Binary
open BinaryRelation
open import Cubical.Induction.WellFounded
  renaming (WellFounded to isWellFounded;
            Acc to isAcc)

open import CantorNormalForm.Base


𝟎IsAcc : isAcc _<_ 𝟎
𝟎IsAcc = acc (λ _ ())

mutual

 Lm[fst-acc] : ∀ {a a'} → isAcc _<_ a' → a ≡ a'
   → ∀ {b x} → isAcc _<_ b → x < a' → (r : x ≥ left b)
   → isAcc _<_ (ω^ x + b [ r ])
 Lm[fst-acc] {a} {a'} (acc ξ) a=a' {b} {x} acᵇ x<a' r = acc goal
   where
    goal : ∀ z → z < ω^ x + b [ r ] → isAcc _<_ z
    goal  𝟎 <₁ = 𝟎IsAcc
    goal (ω^ c + d [ s ]) (<₂ c<y) = Lm[fst-acc] (ξ x x<a') refl IH c<y s
     where
      IH : isAcc _<_ d
      IH = goal d (<-trans (right< c d s) (<₂ c<y))
    goal (ω^ c + d [ s ]) (<₃ c=y d<b) = Lm[snd-acc] (ξ x x<a') c=y acᵇ d<b s

 Lm[snd-acc] : ∀ {a a'} → isAcc _<_ a' → a ≡ a'
   → ∀ {c y} → isAcc _<_ c → y < c → (r : a ≥ left y)
   → isAcc _<_ (ω^ a + y [ r ])
 Lm[snd-acc] {a} {a'} acᵃ a=a' {c} {y} (acc ξᶜ) y<c r = acc goal
  where
   goal : ∀ z → z < ω^ a + y [ r ] → isAcc _<_ z
   goal 𝟎 <₁ = 𝟎IsAcc
   goal (ω^ b + d [ t ]) (<₂ b<a) = Lm[fst-acc] acᵃ a=a' IH (subst (b <_) a=a' b<a) t
    where
     IH : isAcc _<_ d
     IH = goal d (<-trans (right< b d t) (<₂ b<a))
   goal (ω^ b + d [ t ]) (<₃ b=a d<y) = Lm[snd-acc] acᵃ (b=a ∙ a=a') (ξᶜ y y<c) d<y t


<-is-wellfounded : isWellFounded _<_
<-is-wellfounded 𝟎 = 𝟎IsAcc
<-is-wellfounded (ω^ a + b [ r ]) = acc goal
 where
  goal : ∀ z → z < ω^ a + b [ r ] → isAcc _<_ z
  goal 𝟎 <₁ = 𝟎IsAcc
  goal (ω^ c + d [ s ]) (<₂ c<a) =
    Lm[fst-acc] (<-is-wellfounded a) refl IH c<a s
   where
    IH : isAcc _<_ d
    IH = goal d (<-trans (right< c d s) (<₂ c<a))
  goal (ω^ c + d [ s ]) (<₃ c=a d<b) =
    Lm[snd-acc] (<-is-wellfounded a) c=a (<-is-wellfounded b) d<b s


open WFI <-is-wellfounded
  renaming (induction to wf-ind)
  public
