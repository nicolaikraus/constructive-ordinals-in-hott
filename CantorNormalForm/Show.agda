{-# OPTIONS --cubical --safe #-}
module CantorNormalForm.Show where

open import CantorNormalForm.Base
open import Agda.Builtin.String

show : Cnf → String
show 𝟎 = "0"
show (ω^ 𝟎 + 𝟎 [ _ ]) = "1"
show (ω^ 𝟎 + (ω^ 𝟎 + 𝟎 [ _ ]) [ _ ]) = "2"
show (ω^ (ω^ 𝟎 + 𝟎 [ _ ]) + 𝟎 [ _ ]) = "ω"
show (ω^ a + 𝟎 [ _ ]) = primStringAppend "ω^(" (primStringAppend (show a) ")")
show (ω^ (ω^ 𝟎 + 𝟎 [ _ ]) + b [ _ ]) =
  primStringAppend
    "ω"
    (primStringAppend " + " (show b))
show (ω^ a + b [ _ ]) =
  primStringAppend
    (primStringAppend "ω^(" (show a))
    (primStringAppend ") + " (show b))
