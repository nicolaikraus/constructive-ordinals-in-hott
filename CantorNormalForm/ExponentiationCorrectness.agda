----------------------------------------------------------------------------------------------------
-- Exponentiation of Cantor Normal Forms
----------------------------------------------------------------------------------------------------

{-# OPTIONS --cubical --safe #-}

module CantorNormalForm.ExponentiationCorrectness where

open import Cubical.Foundations.Prelude
open import Cubical.Foundations.Function
open import Cubical.Data.Sigma
open import Cubical.Data.Sum
open import Cubical.Data.Empty
  renaming (rec to ⊥-rec)
open import Cubical.Relation.Nullary

open import CantorNormalForm.Base
open import CantorNormalForm.Addition
open import CantorNormalForm.Subtraction
open import CantorNormalForm.Multiplication
open import CantorNormalForm.Exponentiation
open import CantorNormalForm.Arithmetic
open import CantorNormalForm.Classifiability
open import Abstract.Arithmetic _<_ _≤_ public


>𝟏^r-preserves-lim : ∀ {a} c → c > 𝟏 → ∀ f f↑ → a is-lim-of (f , f↑)
                   → (c ^ a) is-sup-of (λ i → c ^ f i)
>𝟏^r-preserves-lim {a} c c>𝟏 f f↑ (f≤a , a-min-over-f) = c^f≤c^a , c^a-min-over-c^f
 where
  c>𝟎 : c > 𝟎
  c>𝟎 = <-trans <₁ c>𝟏
  c^f≤c^a : ∀ i → c ^ f i ≤ c ^ a
  c^f≤c^a i = ^r-is-≤-monotone c (f i) a c>𝟎 (f≤a i)
  c^a-min-over-c^f : ∀ x → (∀ i → c ^ f i ≤ x) → c ^ a ≤ x
  c^a-min-over-c^f x c^f≤x = c^a≤x
   where
    f1>𝟎 : f 1 > 𝟎
    f1>𝟎 = ≤∘<-in-< 𝟎≤ (f↑ 0)
    c<c^f1 : c ≤ c ^ f 1
    c<c^f1 = ≤^r c (f 1) f1>𝟎
    x>𝟎 : x > 𝟎
    x>𝟎 = <∘≤-in-< c>𝟎 (≤-trans c<c^f1 (c^f≤x 1))
    y : Cnf
    y = {!!} -- log x c x>𝟎 c>𝟏
    f≤y : ∀ i → f i ≤ y
    f≤y = {!!} -- greatest-log x c (f i) x>𝟎 c>𝟏 (c^f≤x i)
    a≤y : a ≤ y
    a≤y = a-min-over-f y f≤y
    c^a≤c^y : c ^ a ≤ c ^ y
    c^a≤c^y = ^r-is-≤-monotone c a y c>𝟎 a≤y
    c^y≤x : c ^ y ≤ x
    c^y≤x = {!!} -- log-spec x c x>𝟎 c>𝟏
    c^a≤x : c ^ a ≤ x
    c^a≤x = ≤-trans c^a≤c^y c^y≤x

^r-preserves-sup : ∀ {a} c → c > 𝟎 → ∀ f f↑ → a is-lim-of (f , f↑)
                 → (c ^ a) is-sup-of (λ i → c ^ f i)
^r-preserves-sup {a} c@(ω^ 𝟎 + 𝟎 [ _ ]) <₁ f f↑ (f≤a , a-min-over-f) = c^f≤c^a , c^a-min-over-c^f
 where
  c^f≤c^a : ∀ i → c ^ f i ≤ c ^ a
  c^f≤c^a i = inr (𝟏^-spec a ∙ sym (𝟏^-spec (f i)))
  c^a-min-over-c^f : ∀ x → (∀ i → c ^ f i ≤ x) → c ^ a ≤ x
  c^a-min-over-c^f x c^f≤x = c^a≤x
   where
    c^a≤x : c ^ a ≤ x
    c^a≤x = subst (_≤ x) (𝟏^-spec (f 0) ∙ sym (𝟏^-spec a)) (c^f≤x 0)
^r-preserves-sup c@(ω^ 𝟎 + (ω^ _ + _ [ _ ]) [ _ ]) <₁ = >𝟏^r-preserves-lim c (<₃ refl <₁)
^r-preserves-sup c@(ω^ (ω^ _ + _ [ _ ]) + _ [ _ ]) <₁ = >𝟏^r-preserves-lim c (<₂ <₁)

^-is-exp-with-base : ∀ c → (c ^_) is-exp-with-base c
^-is-exp-with-base c = case-zero , case-suc , case-lim , case-lim₀
 where
  case-zero : ∀ a b → is-zero a → b is-suc-of a → c ^ a ≡ b
  case-zero a b a-is-zero b-is-succ-a =
     c ^ a
    ≡⟨ cong (c ^_) (is-zero→≡𝟎 a-is-zero) ⟩
     c ^ 𝟎
    ≡⟨ refl ⟩
     𝟏
    ≡⟨ cong fst (suc-unique 𝟎 (𝟏 , +𝟏-calc-suc 𝟎) (b , b-is-suc-0)) ⟩
     b ∎
   where
    b-is-suc-0 : b is-suc-of 𝟎
    b-is-suc-0 = subst (b is-suc-of_) (is-zero→≡𝟎 a-is-zero) b-is-succ-a

  case-suc : ∀ a b → a is-suc-of b → c ^ a ≡ c ^ b · c
  case-suc a b a-is-suc-of-b =
     c ^ a
    ≡⟨ cong (c ^_) (sym (suc→+𝟏 a-is-suc-of-b)) ⟩
     c ^ (b + 𝟏)
    ≡⟨ ^+𝟏-spec c b ⟩
     c ^ b · c ∎

  case-lim : ∀ a b f f↑ → a is-lim-of (f , f↑)
           → ¬ is-zero c → b is-sup-of (λ i → c ^ f i) → c ^ a ≡ b
  case-lim a b f f↑ a-is-lim-of-f c-is-not-zero b-is-sup-of-c^f =
     c ^ a
    ≡⟨ sup-unique c^a-is-sup-of-c^f b-is-sup-of-c^f ⟩
     b ∎
   where
    c>𝟎 : c > 𝟎
    c>𝟎 = not-zero-is->𝟎 c-is-not-zero
    c^a-is-sup-of-c^f : (c ^ a) is-sup-of (λ i → c ^ f i)
    c^a-is-sup-of-c^f = ^r-preserves-sup c c>𝟎 f f↑ a-is-lim-of-f

  case-lim₀ : ∀ a f f↑ → a is-lim-of (f , f↑) → is-zero c → c ^ a ≡ c
  case-lim₀ a f f↑ a-is-lim-of-f c-is-zero =
     c ^ a
    ≡⟨ cong (_^ a) (is-zero→≡𝟎 c-is-zero) ⟩
     𝟎 ^ a
    ≡⟨ 𝟎^-spec a (lim≢𝟎 ∣ (f , f↑) , a-is-lim-of-f ∣) ⟩
     𝟎
    ≡⟨ sym (is-zero→≡𝟎 c-is-zero) ⟩
     c ∎
