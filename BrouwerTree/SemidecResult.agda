{-# OPTIONS --cubical --termination-depth=4 --allow-exec #-}
module Brouwer.SemidecResult where

open import Cubical.Foundations.Prelude
open import Cubical.Foundations.HLevels
open import Cubical.Data.Nat
open import Cubical.Data.Sigma
open import Cubical.Relation.Nullary.Properties

open import Cubical.HITs.PropositionalTruncation
open import Cubical.HITs.PropositionalTruncation.Properties
  renaming (rec to ∥-∥rec)

open import Brouwer.Base
open import Sierpinski

-- ## A semidecidability result

{- The function sd⟨_≥ω·_+_⟩ implements the following claim:
   For a cantor normal form below ω² (written as ω·n + k) and any Brouwer ordinal x, it is semidecidable whether x ≥ ω·n+k.
-}

mutual

  sd⟨_≥ω·_+_⟩ : Brw → ℕ → ℕ → Sierpinski

  sd⟨ zero ≥ω· zero + zero ⟩ = ⊤
  sd⟨ zero ≥ω· zero + suc k ⟩ = ⊥
  sd⟨ zero ≥ω· suc n + k ⟩ = ⊥

  sd⟨ succ x ≥ω· zero + zero ⟩ = ⊤
  sd⟨ succ x ≥ω· suc n + zero ⟩ = sd⟨ x ≥ω· suc n + zero ⟩
  sd⟨ succ x ≥ω· n + suc k ⟩ = sd⟨ x ≥ω· n + k ⟩

  sd⟨ limit f {f↑} ≥ω· zero + zero ⟩ = ⊤
  sd⟨ limit f {f↑} ≥ω· suc n + zero ⟩ = ⋁ λ m → sd⟨ f m ≥ω· n + zero ⟩
  sd⟨ limit f {f↑} ≥ω· n + suc k ⟩ = sd⟨ limit f {f↑} ≥ω· n + k ⟩

  sd⟨ bisim f {f↑} g {g↑} f≈g i ≥ω· zero + zero ⟩ = ⊤
  sd⟨ bisim f {f↑} g {g↑} (f≲g , g≲f) i ≥ω· suc n + zero ⟩ =
    bisimilar-implies-⋁-equal
      {s  = λ m → sd⟨ f m ≥ω· n + zero ⟩}
      {s' = λ m → sd⟨ g m ≥ω· n + zero ⟩}
      (λ l → ∥-∥rec propTruncIsProp (λ { (k , gl≤fk) → ∣ k , sd-mono₁ n zero gl≤fk ∣ }) (g≲f l))
      (λ l → ∥-∥rec propTruncIsProp (λ { (k , fl≤gk) → ∣ k , sd-mono₁ n zero fl≤gk ∣ }) (f≲g l))
      i

  sd⟨ bisim f {f↑} g {g↑} f≈g i ≥ω· n + suc k ⟩ = sd⟨ bisim f {f↑} g {g↑} f≈g i ≥ω· n + k ⟩

  sd⟨ trunc x y p q i j ≥ω· n + k ⟩ = isSet→SquareP (λ _ _ → isSetSierpinski)
                                                    (λ j → sd⟨ p j ≥ω· n + k ⟩)
                                                    (λ j → sd⟨ q j ≥ω· n + k ⟩)
                                                    (λ j → sd⟨ x ≥ω· n + k ⟩)
                                                    (λ j → sd⟨ y ≥ω· n + k ⟩) i j


  sd-mono₁ : ∀ n k {y x} → y ≤ x → sd⟨ y ≥ω· n + k ⟩ ≼ sd⟨ x ≥ω· n + k ⟩

  sd-mono₁ zero zero {y} {x} =
    Brw-ind (λ x → y ≤ x → sd⟨ y ≥ω· zero + zero ⟩ ≼ sd⟨ x ≥ω· zero + zero ⟩)
            (λ _ → isPropΠ λ _ → isSetSierpinski _ _)
            (λ _ → everything-under-⊤)
            (λ _ _ → everything-under-⊤)
            (λ _ _ → everything-under-⊤)
            x

  sd-mono₁ (suc n) zero =
    ≤-ind (λ {y x} y≤x → sd⟨ y ≥ω· (suc n) + zero ⟩ ≼ sd⟨ x ≥ω· (suc n) + zero ⟩)
          (λ _ → isSetSierpinski _ _)
          everything-over-⊥ 
          ≼-trans
          (λ {y x} y≤x e → e)
          (λ {x} f {f↑} {k} x≤fk x≥ωn→fk≥ωsn → 
            sd⟨ x ≥ω· suc n + zero ⟩
              ≼⟨ x≥ωn→fk≥ωsn ⟩
            sd⟨ f k ≥ω· suc n + zero ⟩
              ≼⟨ sd-mono₂ n zero (f k) ⟩
            sd⟨ f k ≥ω· n + zero ⟩
              ≼⟨ ≼-cocone-simple k ⟩
            ⋁ (λ m → sd⟨ f m ≥ω· n + zero ⟩)
              ≼∎
          )
          λ f {f↑} {y} f≤y fk≥→y≥ fk≥→fsk≥ → 
            ⋁ (λ k → sd⟨ f k ≥ω· n + zero ⟩)
              ≼⟨ {!!} ⟩ -- interesting case: we need to use that the limit of a sequence reaches the next limit ordinal. This needs a couple of technical lemmas. Currently, it can't work: We have no formal relationship between n and k ∈ ℕ. 
            sd⟨ y ≥ω· suc n + zero ⟩
              ≼∎
  
  sd-mono₁ zero (suc k) = 
    ≤-ind (λ {y x} y≤x → sd⟨ y ≥ω· zero + suc k ⟩ ≼ sd⟨ x ≥ω· zero + suc k ⟩)
          (λ _ → isSetSierpinski _ _)
          everything-over-⊥
          ≼-trans
          (λ {y x} y≤x → {!!})
          {!!}
          λ f {f↑} {y} f≤y → {!!}

  sd-mono₁ (suc n) (suc k) = 
    ≤-ind (λ {y x} y≤x → sd⟨ y ≥ω· suc n + suc k ⟩ ≼ sd⟨ x ≥ω· suc n + suc k ⟩)
          (λ _ → isSetSierpinski _ _)
          everything-over-⊥
          ≼-trans
          (λ {y x} y≤x → {!!})
          {!!}
          {!!}

  -- a ≼ b ↔ (a ≡ ⊤ -> b ≡ ⊤)

  sd-mono₂ : ∀ n k x → sd⟨ x ≥ω· suc n + k ⟩ ≼ sd⟨ x ≥ω· n + k ⟩
  sd-mono₂ = {!!}

  sd-mono₃ : ∀ n k x → sd⟨ x ≥ω· n + suc k ⟩ ≼ sd⟨ x ≥ω· n + k ⟩
  sd-mono₃ = {!!}


  -- want later: sd⟨ x ≥ω· n + k ⟩ ≡ ⊤   ↔   (cnf→brw ...) ≤ x


{-

Previous too optimistic semidec attempts

SemiDec-succ : ∀ x y → SemiDec (x < y) → SemiDec (succ x < succ y)
SemiDec-succ x y (s , l , r) = s , (λ sx≤sy → l (≤-succ-mono⁻¹ sx≤sy)) , λ s≡⊤ → ≤-succ-mono (r s≡⊤)

SemiDec-succ-≤ : ∀ x y → SemiDec (x ≤ y) → SemiDec (succ x ≤ succ y)
SemiDec-succ-≤ x y (s , l , r) = s , (λ sx≤sy → l (≤-succ-mono⁻¹ sx≤sy)) , λ s≡⊤ → ≤-succ-mono (r s≡⊤)

SemiDec-below-limit : ∀ x (f : ℕ → Brw) {f↑} → (∀ n → SemiDec (x < (f n))) → SemiDec (x < limit f {f↑})
SemiDec-below-limit x f {f↑} sdx≤f = (⋁ (λ n → fst (sdx≤f n))) ,
                                     ((λ x<lf → ∥-∥rec (isSetSierpinski (⋁ (λ n → fst (sdx≤f n))) ⊤)
                                                      (λ { (n , x<fn) → contains-⊤-implies-is-⊤ _ ∣ (n , fst (snd (sdx≤f n)) x<fn) ∣  })
                                                      (below-limit-lemma x f {f↑} x<lf)) ,
                                     λ p → ∥-∥rec <-trunc (λ { (n , p) → ≤-cocone f {f↑} {n} (snd (snd (sdx≤f n)) p) }) (is-⊤-implies-contains-⊤ (λ n → fst (sdx≤f n)) p))

{-
SemiDec-limit-limit : ∀ (f : ℕ → Brw) {f↑} (g : ℕ → Brw) {g↑} → (∀ n → SemiDec (limit f {f↑} ≤ g n)) → SemiDec (limit f {f↑} ≤ limit g {g↑})
SemiDec-limit-limit f {f↑} g {g↑} sd = (⋁ λ n → fst (sd n)) ,
                                       (λ lf≤lg → {!lim≤lim→weakly-bisimilar f g lf≤lg!}) ,
                                       {!!}

SemiDec-limit-limit : ∀ (f : ℕ → Brw) {f↑} (g : ℕ → Brw) {g↑} → (∀ k → SemiDec (limit f {f↑} ≤ g k)) → (∀ k → SemiDec (f k ≤ limit g {g↑}))  → SemiDec (limit f {f↑} ≤ limit g {g↑})
SemiDec-limit-limit f {f↑} g {g↑} sdf≤g sdf≤lg = (⋁ (λ n → fst (sdf≤g n))) ,
                                                 (λ lf≤lg → {!snd (!}) ,
                                                 (λ p → weakly-bisimilar→lim≤lim f g λ k → ∥-∥rec propTruncIsProp (λ { (n  , q) → ∣ n , ≤-trans (≤-cocone-simple f) (snd (snd (sdf≤g n)) q) ∣ }) (is-⊤-implies-contains-⊤ _ p))
-}

SemiDec-limit-limit : ∀ (f : ℕ → Brw) {f↑} (g : ℕ → Brw) {g↑} → (∀ n → SemiDec (limit f {f↑} < g n)) → SemiDec (limit f {f↑} < limit g {g↑})
SemiDec-limit-limit f {f↑} g {g↑} sd = (⋁ λ n → fst (sd n)) ,
                                       (λ lf<lg → ∥-∥rec (isSetSierpinski _ _) (λ { (k , lf<gk) → contains-⊤-implies-is-⊤ _ ∣ k , fst (snd (sd k)) lf<gk ∣ }) (below-limit-lemma (limit f) g lf<lg)) ,
                                       λ p → ∥-∥rec <-trunc (λ { (n , q) → <∘≤-in-< (snd (snd (sd n)) q) (≤-cocone-simple g) }) (is-⊤-implies-contains-⊤ _ p)



semidecidable⟨<⟩ : ∀ x y → SemiDec (x < y)
semidecidable⟨<⟩ = Brw-ind (λ x → (y : Brw) → SemiDec (x < y)) (λ x → isPropΠ (λ y → isPropSemiDec <-trunc))
                           (Brw-ind (λ y → SemiDec (zero < y)) (λ y → isPropSemiDec <-trunc)
                                    (Sierpinski.⊥ , (λ z<z → ⊥.rec (<-irreflexive _ z<z)) , (λ ⊥≡⊤ → ⊥.rec (⊥≠⊤ ⊥≡⊤)))
                                    (λ _ → ⊤ , (λ _ → refl) , λ _ → zero<suc)
                                    (λ _ → ⊤ , (λ _ → refl) , λ _ → zero<lim))
                           (λ {x} ih → Brw-ind (λ y → SemiDec (succ x < y)) (λ y → isPropSemiDec <-trunc)
                                               (Sierpinski.⊥ , (λ sx≤z → ⊥.rec (succ≰zero sx≤z)) , λ ⊥≡⊤ → ⊥.rec (⊥≠⊤ ⊥≡⊤))
                                               (λ {y} ih' → SemiDec-succ x y (ih y))
                                               (λ {f} {f↑} ih' → SemiDec-below-limit (succ x) f {f↑} (λ n → ih' n)))
                           λ {f} {f↑} ih → Brw-ind (λ y → SemiDec (limit f < y)) (λ y → isPropSemiDec <-trunc)
                                                   (Sierpinski.⊥ , (λ lf<z → ⊥.rec (x≮zero lf<z)) , (λ ⊥≡⊤ → ⊥.rec (⊥≠⊤ ⊥≡⊤)))
                                                   (λ {y} ih → {!lim<sx→lim≤x f y {f↑}!}) -- (λ {x} (s , l , r) → (s , (λ lf<sx → l {!lf<sx!}) , {!!}))
                                                   λ {g} {g↑} ih' → SemiDec-limit-limit f g (ih')


semidecidable⟨≤⟩ : ∀ x y → SemiDec (x ≤ y)
semidecidable⟨≤⟩ = Brw-ind (λ x → (y : Brw) → SemiDec (x ≤ y)) (λ x → isPropΠ (λ y → isPropSemiDec ≤-trunc))
                           (λ y → ⊤ , (λ _ → refl) , λ _ → ≤-zero)
                           (λ {x} ih → Brw-ind (λ y → SemiDec (succ x ≤ y)) (λ y → isPropSemiDec ≤-trunc)
                                               (Sierpinski.⊥ , (λ sx≤z → ⊥.rec (succ≰zero sx≤z)) , λ ⊥≡⊤ → ⊥.rec (⊥≠⊤ ⊥≡⊤))
                                               (λ {y} ih' → SemiDec-succ-≤ x y (ih y))
                                               (λ {f} {f↑} ih' → SemiDec-below-limit x f {f↑} (λ n → ih' n)))
                           λ {f} {f↑} ih → Brw-ind (λ y → SemiDec (limit f ≤ y)) (λ y → isPropSemiDec ≤-trunc)
                                                   (Sierpinski.⊥ , (λ lf≤z → ⊥.rec (lim≰zero lf≤z) ) , (λ ⊥≡⊤ → ⊥.rec (⊥≠⊤ ⊥≡⊤)))
                                                   (λ {x} (s , l , r) → s , (λ lf≤sx → l (lim≤sx→lim≤x f x lf≤sx)) , λ s≡⊤ → ≤-succ-incr (r s≡⊤))
                                                   (λ {g} {g↑} ih' → {!!})
-}
