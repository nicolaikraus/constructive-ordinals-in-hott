----------------------------------------------------------------------------------------------------
-- Extensional Wellfounded orders
----------------------------------------------------------------------------------------------------

{-# OPTIONS --cubical --erasure #-}

module ExtensionalWellfoundedOrder.Everything where

{- Definition -}
open import ExtensionalWellfoundedOrder.Base public

{- Arithmetic operations -}
open import ExtensionalWellfoundedOrder.Arithmetic public

{- Classifiability -}
open import ExtensionalWellfoundedOrder.Classifiability public
